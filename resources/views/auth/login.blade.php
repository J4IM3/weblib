@extends('layouts.app')
@section('content')
<div class="conteiner_header" style="background-image: url('{{ asset('images/reforma_foto.jpeg') }}')">

    <div class="container">
        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                    <div class="col-sm-8 offset-md-3 login-section-wrapper ">
                        <div class="login-wrapper my-auto">
                            <form method="POST" action="{{ route('login') }}" class="login">
                                @csrf
                                <h2>La Proveedora</h2>
                                <div class="inputs">
                                    <i class="fas fa-user"></i>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="inputs">
                                    <i class="fas fa-key"></i>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Password" autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary btn-block" type="submit">Entrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
