<div class="col-md-12 row">
    <div class="col-md-6">
        <label class="sr-only" for="inlineFormInputGroup">Nombre</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-user"></i></div>
            </div>
            <input type="text" class="form-control item-name" name="name" required id="inlineFormInputGroup" placeholder="Ingresar nombre">
            
        </div>
        <p class="item-errors-name msg-errors" ></p>
        </div>

        <div class="col-md-6">
        <label class="sr-only" for="inlineFormInputGroup">Email</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-at"></i></div>
            </div>
            <input type="text" class="item-email form-control" required name="email" id="inlineFormInputGroup" placeholder="Ingresar email">
        </div>
        <p class="item-errors-email msg-errors" ></p>
        </div>

        <div class="col-md-6 section-password">
        <label class="sr-only" for="inlineFormInputGroup">Password</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-user-secret"></i></div>
            </div>
            <input type="password" class="form-control" required name="password" id="inlineFormInputGroup" placeholder="Ingresar contraseña">
        </div>
        <p class="item-errors-password msg-errors" ></p>
        </div>

        <div class="col-md-6">
        <label class="sr-only" for="inlineFormInputGroup">Rol</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-key"></i></div>
            </div>
            <select name="rol" id="rol " required class="form-control item-rol">
                <option value="">Seleccionar una opción</option>
                <option value="admin">Administrador</option>
                <option value="adminGeneral">Administrador general</option>
                <option value="adminPape">Administrador Papeleria</option>
                <option value="adminLib">Administrador Libreria</option>
                <option value="mostrador">General</option>
            </select>
        
        </div>
        <p class="item-errors-rol msg-errors" ></p>
        </div>

    </div>
