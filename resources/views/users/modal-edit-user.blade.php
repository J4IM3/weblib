<div class="modal" tabindex="-1" role="dialog" id="modal-edit-users">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><p id="title_modal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" id="form-user-edit" name="form-edit-user">
      <div class="modal-body">   
        <input type="text" class="item-id" name="id" hidden>         
        @include('users.fields')
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btnFetch">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
      </form>
    </div>
  </div>
</div>