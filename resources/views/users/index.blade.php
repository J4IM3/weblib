@extends('layouts.app')
@section('content')
<div class="container">
    <h4>Listado de usuarios</h4>
    <button class="btn btn-success" id="new-user">Crear usuario</button>
    <br>
    <br>
    <table class="table table-bordered table-striped" id="table_users">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Acciones</th>
            </tr>
        </thead>
    </table>

    @include('users.modal-nuevo-user')
    @include('users.modal-edit-user')
</div>
@endsection