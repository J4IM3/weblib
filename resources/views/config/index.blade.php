@extends('layouts.app')
@section('content')
<div class="container">
    <h4>Configuración del sistema </h4>

    <div class="col-md-12">
        <div class="card col-md-4">
            <div class="card-header">
            </div>
            <div class="card-body">
            <a href="{{ route('find-images') }} ">Actualizar articulos - imagenes</a>
            @include('layouts.messages')
            </div>
        </div>

        <div class="card col-md-4">
            <div class="card-header">
            </div>
            <div class="card-body">
                <a href="{{ route('insertar_articulos') }} ">Importar articulos de papeleria</a>
                @include('layouts.messages')
            </div>

            <div class="card-body">
                <a href="{{ route('actualizar-descripcion') }} ">Actualizar nombre alterno de articulos</a>
                @include('layouts.messages')
            </div>
        </div>
    </div>

</div>
@endsection
