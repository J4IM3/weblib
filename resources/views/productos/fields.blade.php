<input type="hidden" class="form-control item-id" name="id" id="formGroupExampleInput" placeholder="id">
<div class="form-group">
    <label for="formGroupExampleInput">Isbn</label>
    <input type="text" class="form-control item-isbn" required name="isbn" id="formGroupExampleInput" placeholder="Ingresar ISBN">
</div>

<div class="form-group">
    <label for="formGroupExampleInput">Codigo</label>
    <input type="text" class="form-control item-codigo" required name="codigo" id="formGroupExampleInput" placeholder="Ingresar codigo libro">
</div>

<div class="form-group">
    <label for="formGroupExampleInput">Titulo</label>
    <input type="text" class="form-control item-titulo" required name="titulo" id="formGroupExampleInput" placeholder="Ingresar titulo">
</div>

<div class="form-group">
    <label for="formGroupExampleInput2">Sinopsis</label>
    <p><textarea name="sinopsis" class="form-control item-sinopsis" rows="5" placeholder="Ingresar sinopsis"></textarea></p>
</div>

<div class="modal-footer">
    <button type="submit" class="btn btn-primary">Guardar</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
</div>