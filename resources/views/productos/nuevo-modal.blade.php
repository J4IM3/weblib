<div class="modal" tabindex="-1" id="new-sinopsis-modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nueva sinopsis</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="form-new-sinopsis">
                    @include('productos.fields')
                </form>

            </div>

        </div>
    </div>
</div>