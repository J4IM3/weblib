@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="#" class="btn btn-primary new-sinopsis ">Nueva sinopsis</a>
        <button class="btn btn-success getData">Buscar sinopsis</button>
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}"/>
        <br><br>
        @include('productos.nuevo-modal')
        @include('productos.show-sinopsis')
        @include('productos.editar')
        <table class="table table-bordered table-striped" id="table-books">

            <thead>
            <tr>
                <th>Id</th>
                <th >Isbn</th>
                <th >Isbn2</th>
                <th >Titulo</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
        </table>
    </div>
    @endsection
