@extends('layouts.app')
@section('content')

    <div class="container">

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <h2>Crear nueva sinopsis</h2>

        <form action="{{ route('guardar') }}">
            <div class="form-group">
                <label for="formGroupExampleInput">Isbn</label>
                <input type="text" class="form-control" name="isbn" id="formGroupExampleInput" placeholder="Ingresar ISBN">
            </div>

            <div class="form-group">
                <label for="formGroupExampleInput2">Sinopsis</label>
                <p><textarea name="sinopsis" class="form-control" rows="5" placeholder="Ingresar sinopsis"></textarea></p>
            </div>

            <div class=" float-right">
                <a href="{{ url('/') }}" class="btn btn-primary" >Regresar</a>
                <button class="btn btn-success">Guardar</button>
            </div>
        </form>
    </div>
    @endsection
