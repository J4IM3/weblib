@extends('layouts.app')
@section('content')
    <div class="container" style="max-width: 200%;">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{ asset('banners/banner1.jpeg') }}" height="400" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('banners/banner2.jpeg') }}" height="400" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('banners/banner3.jpeg') }}" height="400" alt="First slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <div class="container">
        <br><br>
        <div class="row">
        <div class="col-lg-3 col-md-12 card-main">
            <a href="{{ route('tl-libreria') }}">
                <div class="card-section card-section-second border rounded ml-4 mr-4">
                    <div class="card-header card-header-second rounded">
                        <h2 class="card-header-title mb-3 mt-1 text-white">Agregar Sinopsis</h2>
                        <p class="mb-0 text-white">
                            <i class="fas fa-book mr-2 text-white pb-3"></i>
                            Sinopsis: {{ $totalSinopsis }}
                        </p>
                    </div>
                </div>
            </a>
        </div>

            <div class="col-lg-3 col-md-12 card-main">
                <a href="{{ route('papeleria') }}">
                    <div class="card-section card-section-second border rounded ml-4 mr-4">
                        <div class="card-header card-header-second rounded">
                            <h2 class="card-header-title mb-3 mt-1 text-white">Articulos Papeleria</h2>
                            <p class="mb-0 text-white">
                                <i class="fas fa-book mr-2 text-white pb-3"></i>
                                Articulos Papeleria: {{ $totalArticulosPapeleria }}
                            </p>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-12 card-main">
                <a href="">
                    <div class="card-section card-section-second border rounded ml-4 mr-4">
                        <div class="card-header card-header-second rounded">
                            <h2 class="card-header-title mb-3 mt-1 text-white">Articulos Virtuales</h2>
                            <p class="mb-0 text-white">
                                <i class="fas fa-book mr-2 text-white pb-3"></i>
                                Articulos virtuales: {{ $totalArticulosVirtuales }}
                            </p>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-12 card-main">
                <a href="">
                    <div class="card-section card-section-second border rounded ml-4 mr-4">
                        <div class="card-header card-header-second rounded">
                            <h2 class="card-header-title mb-3 mt-1 text-white">Departamentos Papeleria</h2>
                            <p class="mb-0 text-white">
                                <i class="fas fa-book mr-2 text-white pb-3"></i>
                                Departamentos: {{ $totalDepartamentos }}
                            </p>
                        </div>
                    </div>
                </a>
            </div>
    </div>
    @endsection
