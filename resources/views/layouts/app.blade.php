<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/js.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style>
    .navbar-light .navbar-brand{
        font-size: 20px;
    }
    .navbar-light .navbar-brand{
        color: white !important;
    }
    .nav-color-text{
        font-size: 18px;
        color:white !important;
    }
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light " style="background-color: #003B5A; color: white;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        @else
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="nav-item active">
                                        <a class="nav-link nav-color-text" href="#">Inicio <span class="sr-only">(current)</span></a>
                                    </li>

                                    <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle nav-color-text" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Sistema
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{  route('users') }}">
                                                    <span style="font-size: 20px; color: Dodgerblue;"><i class="fab fa-pied-piper-pp"></i></span>&nbsp Usuarios
                                                </a>

                                                <a class="dropdown-item" href="{{  route('config') }}">
                                                        <span style="font-size: 20px; color: Dodgerblue;"><i class="fas fa-cog"></i></span>&nbsp Configuración
                                                </a>

                                                <a class="dropdown-item" href="{{  route('config') }}">
                                                    <span style="font-size: 20px; color: Dodgerblue;"><i class="fas fa-cog"></i></span>&nbsp Configuración
                                                </a>
                                            </div>
                                        </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle nav-color-text" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Papeleria
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{  route('papeleria') }}">
                                                <span style="font-size: 20px; color: Dodgerblue;"><i class="fab fa-pied-piper-pp"></i></span>&nbsp Listado de articulos de papeleria
                                            </a>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle nav-color-text" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Libreria
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            {{--<a class="dropdown-item" href="{{  route('libreria') }}">
                                                <span style="font-size: 20px; color: Dodgerblue;"><i class="fas fa-users"></i></span>&nbsp Buscar artículo                                            </a>
--}}
                                            <a class="dropdown-item" href="{{ route('tl-libreria')}}">
                                                <span style="font-size: 20px; color: black;">
                                                    <i class="fas fa-users"></i>
                                                </span>&nbsp Listado de articulos tienda en linea
                                            </a>

                                            <a class="dropdown-item" href="{{ route('tl-libreria')}}">
                                                <span style="font-size: 20px; color: Dodgerblue;"><i class="fas fa-users"></i></span>&nbsp Listado de articulos tienda en linea
                                            </a>
                                        </div>
                                    </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle nav-color-text" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Articulos virtuales
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{  route('articulos_virtuales') }}">
                                                <span style="font-size: 20px; color: Dodgerblue;"><i class="fab fa-pied-piper-pp"></i></span>&nbsp Listado de articulos virtuales
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle nav-color-text" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-1">
            @yield('content')
        </main>
    </div>
</body>
</html>
