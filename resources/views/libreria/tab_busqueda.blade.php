<ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Buscar</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Busqueda avanzada</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <br>
            <form action="{{ route('find-book') }}" method="POST" class="form-row">
                @csrf
                <div class="col-md-5">
                    <select name="option-select-choise" id="" class="form-control">
                        <option value="">Seleccione una opción</option>
                        <option value="code">Código</option>
                        <option value="description">Titulo</option>
                        <option value="editorial">Editorial</option>
                        <option value="isbn">Isbn</option>
                        <option value="autor">Autor</option>
                    </select>
                </div>
                <div class="col-md-5">
                    <input class="form-control" name="article-search" type="text" placeholder="Busca tu libro">
                </div>
                <div class="col-md-2 form-group form-inline">
                    <button class="btn btn-success btn-block" type="submit">Buscar libro </button>
                </div>
            </form>
            @include('layouts.messages')
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            Busqueda avanzada
            @include('libreria.busqueda')
        </div>
    </div>