@extends('layouts.app')
@section('content')
    <div class="container">
        @if(empty($result))
            @include('libreria.tab_busqueda')
        @endif
        @if(!empty($results))
            @include('libreria.resultados')
        @endif
    </div>
@endsection