<h3>Resultados de la busqueda</h3>
<br><br>

<ul style="list-style: none">
    <div class="row">
        @foreach($results as $result)
        <div class="card col-md-3" align="center" style="padding-top: 10px">
                @if(empty($result->artImagesLib[0]))
                    <img class="card-img-result"  align="center" src="{{ asset('images/no-found.png') }}" alt="{{ $result->code}}" >
                    @else
                    <div class="img-wrap"><img src="{{ asset($result->artImagesLib[0]->url_image) }}" style="width: 75%;height: 20vw;"></div>
                @endif

                <p class="sku">Código: {{ $result->code}}</p>
                
                <ul class="list-group list-group-flush">
                    <li class="list-group-item ">
                        <p class="item-precio-articulo" style="min-height:60px;"> {{ $result->description }}</p>
                        <p>
                            <div class="label-rating">Editorial: @if(!empty($result->editorials->name))
                                <a href="{{ route('find-by-editorials',[$result->editorials->code]) }}">  {{ $result->editorials->name }} </a>
                                @endif</div>
                        </p>
                        
                        <div class="label-rating">Genero: @if(!empty($result->sectionsLib->name))
                                <a href="{{ route('find-by-section',[$result->sectionsLib->code]) }}">  {{ $result->sectionsLib->name}} </a>
                            @endif</div>
                            <br><br>
                    </li>
                    <li class="list-group-item">
                            <a href="{{ route("view-details-lib",[$result->code]) }}" class="btn btn-sm btn-primary btn-block">Ver detalle</a>

                    </li>
                </ul>
                <div class="card-body row ">

                </div>
            </div>

            @endforeach
            {{ $results->links() }}
    </div>
</ul>