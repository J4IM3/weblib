@extends('layouts.app')
@section('content')
    <div class="container">
            
            @include('libreria.tab_busqueda')
            <br>
        <h4>Detalles del artículo</h4>
        
        <br>
        <hr style="border: solid red">
        @include('layouts.messages')
        @foreach($books as $book)
        <div class="card mb-3" style="max-width:100%;">
                <div class="row no-gutters">
                  <div class="col-md-5 col-sm-12 ">
                        <br><br>
                        <div class="w3-content w3-display-container">
                            
                                @if($book->artImagesLib != null)
                                @foreach($book->artImagesLib as $images)
                                    <img class="mySlides zoom" src="{{ asset($images->url_image ) }}" style="height: 20vw; margin-left:10% ">
                                    @endforeach
                                <button class="w3-button w3-black w3-display-left plusDivs" data-value="-1">&#10094;</button>
                                <button class="w3-button w3-black w3-display-right plusDivs" data-value="1">&#10095;</button>
                                @else
                                <img class="card-img-detail  " align="center" src="{{ asset('images/no-found.png') }}" alt="{{ $article->description}}" >
                                @endif
                        </div>
                            <br>
                        <form class="form-group" action="{{ route('add-image',[$book->code]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input  required type="file" id="myfile" name="myImage">
                            <button class="btn btn-info" type="submit">Agregar imagen</button>
                        </form>
                        <hr style="border: solid red">
                         <div class="col-md-12 row">

                            <div class="col-md-3 form-group">
                                Código
                            </div>
                            <div class="col-md-9 form-group">
                                {{ $book->code}}
                            </div>
                        
                            <div class="col-md-3 form-group">
                                Descripción
                            </div>
                            <div class="col-md-9 form-group">
                                {{ $book->description}}
                            </div>

                            <div class="col-md-3 form-group">
                                Autor(es)
                            </div>

                            <div class="col-md-9 form-group">
                                {{ $book->autor }}
                            </div>

                            <div class="col-md-3 form-group">
                                Editorial
                            </div>
                            <div class="col-md-9 form-group">
                                @if(!empty($book->editorials->name ))
                            <p class="properties-item"> <a href="{{ route('find-by-editorials',[$book->editorials->code]) }}">{{ $book->editorials->name }}</a>   </p>
                                @endif
                            </div>

                            <div class="col-md-3 form-group">
                                Proveedor   
                            </div>
                            <div class="col-md-9 form-group">
                                @if(!empty($book->proveedoresLib[0]->CardCode ))
                            <p class="properties-item"> <a href="{{ route('find-by-proveedor',[$book->proveedoresLib[0]->CardCode]) }}">{{ $book->proveedoresLib[0]->CardName }}</a>   </p>
                                @endif
                            </div>
                                
                            <div class="col-md-3 form-group">
                                Precio
                            </div>

                            <div class="col-md-7 form-group">
                                    $ {{ $book->price }}
                            </div>

                            <div class="col-md-3 form-group">
                                ISBN
                            </div>
                            
                            <div class="col-md-9 form-group">
                                {{ $book->isbn}}
                            </div>

                            <div class="col-md-4 form-group">
                                Estado disponibilidad
                            </div>

                            <div class="col-md-8 form-group">

                            </div>

                            <div class="col-md-3 form-group">
                                Producto
                            </div>

                            <div class="col-md-9 form-group">
                                Libro
                            </div> 

                            <div class="col-md-3 form-group">
                                Series y colecciones
                            </div>

                            <div class="col-md-9 form-group">
                            </div>
                            
                            <div class="col-md-3 form-group">
                                Tipo de producto
                            </div>
                            
                            <div class="col-md-9 form-group">
                                Libro de pasta blanda (paperback)
                            </div> 

                            <div class="col-md-3 form-group">
                                Grupo de clientes
                            </div>

                            <div class="col-md-9 form-group">
                                
                            </div> 




                            
                         </div>  
                        

                        
                  </div>

                  <div class="col-md-7">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Existencias</p>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sucursal</th>
                                                <th>Stock</th>
                                                <th>Unidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($book->existenciasLib as $item)
                                            <tr>
                                                <th>{{ $item->sucursal }}</th>
                                                <th>{{ $item->OnHand }}</th>
                                                <th>Pieza</th>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                            </div>
                                          
                            <div class="col-md-3 form-group">
                                Tema
                            </div>
                            <div class="col-md-9 form-group">
                                @if(!empty($article->seccion_basic->name ))
                                <p class="properties-item">
                                    <a href="{{ route('get-article-by-matter',[$article->section]) }}">{{ $article->seccion_basic->name }}</a> 
                                </p>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <p class="properties-item">Fecha publicación</p>
                            </div>
                            
                            <div class="col-md-12">

                            </div>
                            
                            <div class="col-md-12">
                                <p class="properties-item">Datos personales autor</p>
                            </div>
                            
                            <div class="col-md-12">                        

                            </div>

                            <div class="col-md-12">
                                <p class="properties-item">Número de páginas</p>
                            </div>
                            
                            <div class="col-md-12">

                            </div>
                            
                            <div class="col-md-12">
                                <p class="properties-item">Alto/Ancho/Grueso</p>
                            </div>
                            
                            <div class="col-md-12">

                            </div>
                            
                            <div class="col-md-12">
                                <p class="properties-item">Palabras claves</p>
                            </div>
                            
                            <div class="col-md-12">

                            </div>
                            
                            <div class="col-md-12">
                                <p class="properties-item">Series y colecciones</p>
                            </div>
                            
                            <div class="col-md-12">

                            </div>
                            
                            
                            <form action="{{ route('form-update-book-detail') }}" id="" method="POST" class="form-row col-md-12">
                                @CSRF
                                <input name="code" type="hidden" value="{{ $book->code }}">
                                
                                <div class="col-md-5">
                                    <p class="properties-item">Bisac</p>
                                </div>
                                
                                <div class="col-md-5">
                                    <select name="bisac" id="" class="form-control form-control-chosen item-basic-list"  data-placeholder="Selecciona una  opción">
                                        @foreach($listBisac as $list)
                                        <option value="{{ $list->id }}" @if($book->bisac == $list->id)) selected @endif>{{ $list->code }} - {{ $list->esp }}</option>
                                        @endforeach
                                    </select>
                                </div>
            
                                <div class="col-md-5">
                                    <p class="properties-item">Tema</p>
                                </div>
                                
                                <div class="col-md-5">
                                    <select name="thema_categorie" id="" class="form-control form-control-chosen item-thema"  data-placeholder="Selecciona una  opción">
                                        @foreach($themas as $list)
                                        <option value="{{ $list->id }}" @if($book->thema_categorie == $list->id)) selected @endif>{{ $list->code }} - {{ $list->level1 }} - {{ $list->level2 }} - {{ $list->description_matter }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                
                                <div class="col-md-5">
                                    <p class="properties-item">Materia</p>
                                </div>
                                
                                <div class="col-md-5">
                                    <select name="matter_id" id="" class="form-control form-control-chosen item-matter_id"  data-placeholder="Selecciona una  opción">
                                        @foreach($librarySection as $list)
                                        <option value="{{ $list->code }}" @if($book->matter_id == $list->code)) selected @endif>{{ $list->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="col-md-12">
                                    <button class="btn btn-info float-right" type="submit">Actualizar</button>
                                </div>

                                <div class="col-md-12">
                                        <p class="properties-item">Sinopsis</p>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <textarea name="sinopsis" id="" class="form-control" rows="10" >@if(!empty($book->sinopsis->sinopsis)){{ $book->sinopsis->sinopsis}}@endif</textarea>
                                    </div>
                                    
                            
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 ">
                    <a class="btn btn-success float-right" href="{{ route('libreria') }}">Regresar</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endforeach
</div>
@endsection