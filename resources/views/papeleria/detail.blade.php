@extends('layouts.app')
@section('content')
    <div class="container">
        @include('papeleria.busqueda')
        <h4>Detalles del artículo </h4>
        <br><br>
        <hr style="border: solid red">
        @include('layouts.messages')

        <div class="card mb-3" style="max-width:100%;">
                <div class="row no-gutters">
                  <div class="col-md-5 col-sm-12 ">
                        <div class="w3-content w3-display-container">
                                @if($article->artImages != null)
                                @foreach($article->artImages as $images)
                                    <img class="mySlides zoom" src="{{ asset($images->url_image ) }}" style="height: 20vw; margin-left:10% ">
                                    @endforeach
                                <button class="w3-button w3-black w3-display-left plusDivs" data-value="-1">&#10094;</button>
                                <button class="w3-button w3-black w3-display-right plusDivs" data-value="1">&#10095;</button>
                                @else
                                <img class="card-img-detail  " align="center" src="{{ asset('images/no-found.png') }}" alt="{{ $article->description}}" >
                                @endif
                        </div>
                        <form class="form-group" action="{{ route('add-image',[$article->code]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input  required type="file" id="myfile" name="myImage">
                            <button class="btn btn-info" type="submit">Agregar imagen</button>
                        </form>
                        
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Sucursal</th>
                                    <th>Stock</th>
                                    <th>Unidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($article->existencias as $item)
                                <tr>
                                <th>{{ $item->sucursal }}</th>
                                <th>{{ $item->OnHand }}</th>
                                <th>{{ $article->main_unit }}</th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                  </div>

                  <div class="col-md-7">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 form-group">
                                Código
                            </div>
                            <div class="col-md-9 form-group">
                                {{ $article->code}}
                            </div>

                            <div class="col-md-3 form-group">
                                    Descripción
                                </div>
                                <div class="col-md-9 form-group">
                                    {{ $article->description}}
                                </div>

                                <div class="col-md-3 form-group">
                                        Código de barras
                                    </div>
                                    <div class="col-md-9 form-group">
                                        {{ $article->codeBars}}
                                    </div>

                                    <div class="col-md-3 form-group">
                                            Linea
                                        </div>
                                        <div class="col-md-9 form-group">
                                                @if(!empty($article->seccion_basic->name ))
                                                <p class="properties-item">
                                                <a href="{{ route('get-article-by-matter',[$article->section]) }}">{{ $article->seccion_basic->name }}</a> 
                                                </p>
                                                    @endif
                                        </div>

                                            <div class="col-md-12 form-group">
                                                <p class="col-md-12 col-sm-12 offset-md-5 offset-sm-6">Precios</p>   
                                                <table class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Unidad</th>
                                                            <th>Menudeo</th>
                                                            <th>Mayoreo</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                        <td>  {{ $article->main_unit}}</td>
                                                        <td>$ {{ $article->precios->menudeo}}</td>
                                                        <td>$ {{ $article->precios->mayoreo}}</td>
                                                        </tr>
                                                        @foreach ($article->unidadesP as $key => $item)
                                                        <tr>
                                                        <td>{{ $key }}</td>
                                                        <td>$ {{ $item['menudeo'] }}</td>
                                                        <td>$ {{ $item['mayoreo'] }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                    
                                                
                                            </div>



                                        @if($article->activate_tl == 1)
                                        <div class="col-md-12">
                                                <hr style="border: 1px solid red;">
                                                 Información tienda en linea
                                                <hr style="border: 1px solid red;">
                                        </div>
                                        <div class="col-md-3 form-group">
                                            Código
                                            </div>
                                            <div class="col-md-9 form-group">
                                                {{ $article->code_tl}}
                                                </div>    

                                                <div class="col-md-3 form-group">
                                                        Descripción alternativa
                                                        </div>
                                                        <div class="col-md-9 form-group">
                                                            {{ $article->description_tl}}
                                                            </div>    

                                                <div class="col-md-3 form-group">
                                                        Departamento
                                                        </div>
                                                        <div class="col-md-9 form-group">
                                                            {{ $article->department->description}}
                                                            </div>    

                                                            <div class="col-md-3 form-group">
                                                                    Unidad 
                                                                    </div>
                                                                    <div class="col-md-9 form-group">
                                                                        {{ $article->unidad_tl}}
                                                                        </div>    
                                        @endif
                        </div>
                      
                    </div>
                  </div>
                </div>
              </div>

        
        
    </div>
    @endsection