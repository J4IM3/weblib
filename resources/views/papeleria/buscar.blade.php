<div class="container">
        @if(empty($result))
            <br><br>
            <form action="{{ route('search-article-pape-cte') }}" class="row" method="POST">
                <div class="col-md-12 row">
                    @csrf
                    <div class="col-md-5 form-group">
                        <input type="text" name="description" placeholder="Introduce la descripción del artículo" class="form-control item-description"  >
                    </div>

                    <div class="col-md-5 form-group">
                            <input type="text" name="proveedor" placeholder="Introduce el proveedor" class="form-control item-proveedor" >
                        </div>

                    <div class="col-md-2 form-group form-inline">
                        <button  class="btn btn-success" type="submit">Buscar</button>
                    </div>
                </div>
            </form>
            @endif
</div>