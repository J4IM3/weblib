@extends('layouts.app')
@section('content')
    <div class="container">
        <h4>Detalles del artículo</h4>

        <br><br>
        <hr style="border: solid red">
        @include('layouts.messages')
            <div class="card">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-block">
                            <h4 class="card-title"></h4>
                            <div class="row">
                                <div class="col-md-5">
                                    <p class="properties-item">Código</p>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">{{ $article->code }}</p>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">Descripción</p>
                                </div>

                                <div class="col-md-6">
                                    <p class="properties-item">{{ $article->description }}</p>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">Código barras</p>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">{{ $article->codeBars }}</p>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">Precio</p>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">
                                        <select name="" id="">
                                            <option value="{{ $article->code }}">{{ $article->main_unit  }}</option>
                                            @foreach($article->unidadesPrecio as $unidades)
                                                <option value="">{{ $unidades->UomName }}</option>
                                                @endforeach
                                        </select>
                                    </p>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">Existencias</p>
                                </div>

                                <div class="col-md-4">
                                    <p class="properties-item">
                                        <select name="" id="" class="form-control select-sucursal-existencia" >
                                            <option value="">Selecciona una sucursal</option>
                                            @foreach($article->existencias as $item)
                                                <option value="{{ $item->sucursal }}" data-existencia="{{ $item->OnHand }}">{{ $item->sucursal }}</option>
                                                @endforeach
                                        </select>
                                    </p>
                                </div>

                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary">
                                         <span class="badge badge-light existencias-sucursal">0</span>
                                        <span class="sr-only"></span>
                                    </button>
                                </div>

                                <div class="col-md-5">
                                    <p class="properties-item">Departamento</p>

                                </div>
                                <div class="col-md-5">
                                    @if(!empty($article->seccion_basic->name ))
                                    <p class="properties-item">{{ $article->seccion_basic->name }}</p>
                                        @endif
                                </div>

                            </div>
                            <form action="{{ route('add-image',[$article->code]) }}" method="POST" enctype="multipart/form-data">
                                @csrf

                            </form>
                            </div>

                    </div>
                    <div class="col-md-6">
                        <div class="w3-content w3-display-container">
                                @foreach($article->artImages as $images)
                                    <img class="mySlides zoom" src="{{ asset($images->url_image ) }}" style="height: 20vw; ">
                                    @endforeach
                                <button class="w3-button w3-black w3-display-left plusDivs" data-value="-1">&#10094;</button>
                                <button class="w3-button w3-black w3-display-right plusDivs" data-value="1">&#10095;</button>
                        </div>
                    </div>
            </div>
                <div class="col-md-12 offset-md-10">
                    <a class="btn btn-success" href="{{ route('papeleria')  }}">Regresar</a>
                </div>

        </div>
    @endsection