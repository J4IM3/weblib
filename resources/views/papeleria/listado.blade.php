@extends('layouts.app')
@section('content')
    <div class="container">
        <h3>Listado tienda en linea Papeleria</h3>
        <br>
        {{--<button class="btn btn-success add-article-tl">Modificar artículo</button>--}}
        <br>
        <br>
        <table class="table table-striped table-bordered" id="table-tl-pape" width="100%">
            <thead>
            <tr>
                <th>Id</th>
                <th>Código Eviciti</th>
                <th>Código Ecommerce</th>
                <th>Nombre alterno</th>
                <th>Departamento</th>
                <th>Unidad</th>
                <th>Acciones</th>
            </tr>
            </thead>
        </table>
        @include('papeleria.nuevo-articulo-papeleria')
    </div>
    @endsection
