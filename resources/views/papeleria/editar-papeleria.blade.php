<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" id="edit-article-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar articulo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="update-article-pape">
            <div class="modal-body">
                <div class="col-md-12">
                    @include('papeleria.fields')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Actualizar</button>
            </div>
            </form>
        </div>
    </div>
</div>