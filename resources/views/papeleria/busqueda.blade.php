<form action="{{ route('find-article-pape') }}" method="POST" class="form-row">
        @csrf
        <div class="col-md-4">
                <select name="option-select-choise" id="" class="form-control">
                    <option value="">Seleccione una opción</option>
                    <option value="code">Código</option>
                    <option value="description">Descripción</option>
                    <option value="proveedor">Proveedor</option>
                    <option value="codeBars">Código de barras</option>
                </select>
        </div>
        <div class="col-md-4">
            <input class="form-control" name="article-search" type="text" placeholder="Busca tu artículo">
        </div>
        <div class="col-md-4 form-group form-inline">
            <button class="btn btn-success" type="submit">Buscar artículo</button>
        </div>
    </form>