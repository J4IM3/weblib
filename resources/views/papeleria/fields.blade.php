<div class="row">
    <div class="col-md-6">
        <label for="codigo_evi">Codigo Proveedor</label>
        <input type="text" class="form-control item-codigo_evi"  name="codigo_evi" placeholder="Código Proveedora" >
    </div>
    <div class="col-md-6">
        <label for="codigo_tl">Codigo Tienda en linea</label>
        <input type="text" class="form-control item-codigo_tl" readonly name="codigo_tl"placeholder="Código Tienda en linea">
    </div>

    <div class="col-md-6">
        <label for="descripcion">Descripción</label>
        <textarea class="form-control item-descripcion" name="descripcion" readonly rows="3"></textarea>
    </div>

    <div class="col-md-6">
        <label for="descripcion">Descripción alterna</label>
        <textarea class="form-control item-descripcion_alterna" name="descripcion_alterna" rows="3"></textarea>
    </div>

    <div class="col-md-4">
        <label for="proveedor">Proveedor</label>
        <input type="text" class="form-control item-proveedor" name="proveedor" placeholder="Proveedor">
    </div>

    <div class="col-md-4">
        <label for="unidad">Unidad secundaria</label>
        <input type="text" class="form-control item-unidad_id" name="unidad_id" placeholder="Unidad">
    </div>

    <div class="col-md-4">
        <label for="codigo_evi">Departamento</label>
        <select name="departamento_id" class="form-control item-departamento_id">
            @foreach($departamentos as $departamento)
                <option value="{{ $departamento->code }}">{{ $departamento->description }}</option>
            @endforeach
        </select>
    </div>

</div>