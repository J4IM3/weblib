<div class="row">
    <div class="col-md-12">
        <label for="codigo_evi">Codigo Proveedora</label>
        <input type="text" class="form-control item-codigo_evi" readonly required name="codigo_evi" placeholder="Código Proveedora" >
    </div>

    {{--<div class="col-md-4">
        <label for="proveedor">Proveedor</label>
        <input type="text" class="form-control item-proveedor" readonly name="proveedor" placeholder="Proveedor">
    </div>--}}

    {{--<div class="col-md-6">
        <label for="descripcion">Descripción</label>
        <textarea class="form-control item-description" readonly name="descripcion"  rows="3"></textarea>
    </div>--}}

    <div class="col-md-12">
        <label for="descripcion_alterna">Nombre alterno</label>
        <textarea class="form-control item-descripcion_alterna" name="descripcion_alterna" rows="3"></textarea>
    </div>

    <div class="col-md-12">
        <label for="descripcion">Descripción del artículo</label>
        <textarea class="form-control item-descripcion" name="descripcion" rows="3"></textarea>
    </div>

    <div class="col-md-6">
        <label for="select-unit">Unidad secundaria</label>
        <input type="text" class="form-control item-unidad_id">
    </div>

    <div class="col-md-6">
        <label for="codigo_evi">Departamento</label>
        <select name="departamento_tl" id="departamento_tl" required class="form-control item-departamento_tl">
            <option value="">Selecciona una opción</option>
            @foreach($departments as $department)
                <option value="{{ $department->code }}"> {{ $department->description }}</option>
                @endforeach
        </select>
    </div>

</div>
