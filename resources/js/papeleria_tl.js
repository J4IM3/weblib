$(document).ready(function () {

    $("#table-tl-pape").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-table-pape-ajax',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'codigo_evi', name: 'codigo_evi' },
            { data: 'codigo_tl', name: 'codigo_tl' },
            { data: 'descripcion_alterna', name: 'descripcion_alterna' },
            { data: 'department.description', name: 'department.description' },
            { data: 'unidad_id', name: 'unidad_id' },
        ],
        "columnDefs": [{
            "targets": 6,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                var icon , title,colorClas;
                if(row.status != 0 )
                {
                    icon = "far fa-check-circle";
                    title = "Desactivar";
                    colorClas ="btn-primary";
                }else{
                    icon = "fas fa-times-circle";
                    title = "Activar";
                    colorClas = "btn-danger";
                }

                return  '<button class="btn btn-info edit-article" ' +
                    'data-val="'+ row +'" ' +
                    'data-toggle="tooltip" title="Editar artículo">' +
                    '<span class="far fa-edit"></span>' +
                    '</span>' +
                    '</button> '+

                    '<button class="btn '+ colorClas +' update-status" ' +
                    'data-code="'+ row.codigo_evi +'" ' +
                    'data-toggle="tooltip" title="'+title+'">' +
                    '<span class="'+icon+'"></span>' +
                    '</span>' +
                    '</button> '
            }
        }
        ]
    });


    $("#table-tl-pape tbody").on('click','.edit-article',function(e){
       e.preventDefault();
        $("#nuevo-articulo-papeleria").modal("show");
        var data_user = $("#table-tl-pape").DataTable();
        var rowData = data_user.row( $(this).parents('tr') ).data();
        $.each(rowData,function (index,value) {

            $(".item-"+index).val(value);
            if (index =="departamento_id")
            {
                $('.item-departamento_tl').val(value);
            }
            if(index == "resenias_pape"){
                $(".item-descripcion").val(value.sinopsis);
            }
        });

       /*$("#edit-article-modal").modal('show');
       var data = new FormData;
       var codigo = $(this).data('code');
       data.append('codigo',codigo);

        $.ajax({
            url: "get-information-article",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $.each(data , function (index,value) {
                    $(".item-"+index).val(value);
                    if (index="departamento_id" ){
                        $("item-departamento_id option:selected").text(value);
                    }
                })
            }
        })*/
    });

    $("#update-article-pape").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData;
        data.append('codigo_evi',$( ".item-codigo_evi" ).val());
        data.append('descripcion_alterna',$( ".item-descripcion_alterna" ).val());
        data.append('unidad_id',$( ".item-unidad_id" ).val());
        data.append('departamento_id',$( ".item-departamento_id" ).val());
        $.ajax({
            url: "update-article-pape",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

                $("#table-tl-pape").DataTable().ajax.reload(null,false);
                $("#edit-article-modal").modal('hide');
                swal.fire({
                    position: 'center',
                    type: "success",
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 1500
                })

            }
        })
    })

    $("#table-tl-pape tbody").on('click','.update-status',function (e) {
        e.preventDefault();
        var code = $(this).data('code');
        var data = new FormData;
        data.append('code',code);
        $.ajax({
            url: "change-status-tl",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $("#table-tl-pape").DataTable().ajax.reload(null,false);
                swal.fire({
                    position: 'center',
                    type: data.success,
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        });
    });

    $(".add-article-tl").on('click',function (e) {
        e.preventDefault();
        $('#add-article-tl')[0].reset();
        $("#nuevo-articulo-papeleria").modal("show");
    });

    $(".item-codigo_evi").focusout(function () {
        var code =  $(".item-codigo_evi").val();
        var data = new FormData;
        data.append('code',code);
        getInfo(data,'get-info-by-code');
        $('#select-unit').empty();
    });

    $("#add-article-tl").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData;
        data.append('code',$( ".item-codigo_evi" ).val());
        data.append('descripcion_tl',$( ".item-descripcion_alterna" ).val());
        data.append('unidad_tl',$(".item-unidad_id" ).val());
        data.append('departamento_id',$( ".item-departamento_tl" ).val());
        data.append('descripcion',$( ".item-descripcion" ).val());
        $.ajax({
            url: "update-article-pape",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $("#table-tl-pape").DataTable().ajax.reload();
                $("#nuevo-articulo-papeleria").modal('hide');
                swal.fire({
                    position: 'center',
                    type: data.success,
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    })
    function getInfo(data,route) {

        $.ajax({
            url: route,
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                var unidad_principal;
                var unidad_tl;
                var description
                if (data.suçccess != 'error')
                {
                    $.each(data,function (index,value)
                    {
                        console.log(index,value)
                        $(".item-"+index).val(value);
                        if (index =="departamento_id")
                        {
                            $('.item-departamento_tl').val(value);
                        }
                    })
                    return true;
                }

                modalEvent(data.success,data.msg)

            }
        })
    }
});
