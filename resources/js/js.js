$(document).ready(function() {
    var ruta = "";
    var method = "";
    var data = "";
    var modal = "";

    $("#table-books").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-table-books-ajax',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'isbn', name: 'isbn' },
            { data: 'isbn2', name: 'isbn2' },
            { data: 'titulo', name: 'titulo' },
        ],
        "columnDefs": [{
            "targets": 4,
            "data": "download_link",
            "render": function(data, type, row, meta) {
                return '<a href="#" class="btn btn-info show-sinopsis" data-sinopsis="' + row.sinopsis + '" data-toggle="tooltip" title="Ver sinopsis"><i class="fas fa-align-justify"></i></a> ' +
                    '<a href="#" class="btn btn-primary edit-sinopsis" data-sinopsis="' + row.sinopsis + '" data-toggle="tooltip" title="Editar sinopsis"><i class="fas fa-edit"></i></a> '

            }
        }]
    });

    $(".new-sinopsis").on('click', function(e) {
        e.preventDefault();
        $("#form-new-sinopsis")[0].reset();
        $("#new-sinopsis-modal").modal('show');
    })

    $("#form-new-sinopsis").on('submit', function(e) {
        e.preventDefault();
        ruta = "new-sinopsis";
        method = "POST";
        data = new FormData(this);
        modal = "#new-sinopsis-modal";
        sendData(ruta, method, data, modal);
    });

    $("#table-books tbody").on('click', '.show-sinopsis', function(e) {
        e.preventDefault();
        var sinopsis = $(this).data('sinopsis');
        $("#show-sinopsis-modal").modal('show');
        $("#parrafo_sinopsis").text(sinopsis);
        console.log(sinopsis)
    });

    $("#table-books tbody").on('click', '.edit-sinopsis', function(e) {
        e.preventDefault();
        var books_table = $("#table-books").DataTable();
        var rowData = books_table.row($(this).parents('tr')).data();
        $("#form-edit-sinopsis-modal").modal('show');
        $.each(rowData, function(index, element) {
            $(".item-" + index).val(element);
        });
    })

    $("#form-edit-sinopsis").on('submit', function(e) {
        e.preventDefault();
        ruta = "edit-sinopsis";
        method = "POST";
        data = new FormData(this);
        modal = "#form-edit-sinopsis-modal";
        sendData(ruta, method, data, modal);
    });

    function sendData(ruta, method, data, modal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.success == 'success')
                {
                    closeModal(modal);
                    modalEvent(data.success, data.msg);
                    $("#table-books").DataTable().ajax.reload();
                    return true;
                }
                modaErrorlEvent(data.success,data.msg);

            }
        });
    }

    function closeModal(id) {
        $(id).modal('hide');
    }

    $(".getData").on('click', function(e) {
        e.preventDefault();

    });

    function buscarResenia(isbn) {
        var fechaSTR = '20190712012452';
        var integradorSTR = 17;
        var llaveSTR = "7fa587697d72392a0761d8084095adb4";
        //var firmaSTR = md5(fechaSTR+integradorSTR+llaveSTR);
        var firmaSTR = "b7431b50392ffb815d1dbc33cde03767";

        $.ajax({
            type: "POST",
            url: "https://pro.librosmexico.mx/webservices/json2/getLibro",
            data: { id_libro: "1", isbn: "9786070726682", fecha: fechaSTR, integrador: integradorSTR, firma: firmaSTR, fecha_colofon_1: "2017-04-01" },
            success: function(response) {
                console.log(response);
            },
            error: function(response) {
                console.log(response);
            }
        });

    }

});