/**
 * Created by Jaime on 04/07/2019.
 */
$(document).ready(function () {
    window.modalEvent = function(type,msg)
    {
        swal({
            position: 'center',
            type: type,
            title: msg,
            showConfirmButton: false,
            timer: 1500
        });
    }

    window.modaErrorlEvent = function(type,msg,showConfirm)
    {
        swal({
            position: 'center',
            type: type,
            title: msg,
            showConfirmButton: true,
        });
    }

});
