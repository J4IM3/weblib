$(document).ready(function(){
    var name = "";
    var email = "";
    var password = "";
    var rol = "";
    var ruta = "";
    $("#table_users").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-users-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'name', name:'name'},
            {data: 'email', name:'email'},
        ],
        "columnDefs": [{
            "targets": 3,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                var icon , title,colorClas;
                if(row.status != 0 )
                {
                    icon = "far fa-check-circle";
                    title = "Desactivar";
                    colorClas ="primary";
                }else{
                    icon = "fas fa-times-circle";
                    title = "Activar";
                    colorClas = "danger";
                }
                return  '<a href="#" class="btn btn-info edit-user"    data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                    '<a href="#" class="btn btn-'+ colorClas +' change-status"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
            }}
        ]
    });

    $("#new-user").on('click',function(e){
        e.preventDefault;
        $("#modal-users").modal('show');
        $("#title_modal").html("Crear usuario");
        $('.section-password').find('input').attr('disabled',false);
    });


    $("#form-user-create").on('submit',function(e){
        e.preventDefault();
        var data = document.forms.namedItem("form-user-create");
        var formData = new FormData(data);
        clearSpanFormUser();
        activar();
        senData("new-user","modal-users",formData);
    });


    $("#table_users tbody").on('click','.edit-user',function(){    
        $("#modal-edit-users").modal('show');
        $("#title_modal").html("Editar usuario");
        var list_users = $("#table_users").DataTable();
         desactivar();
         $('.section-password').find('input').attr('disabled','disabled');
         var rowData = list_users.row( $(this).parents('tr') ).data();
         $.each(rowData,function (index,value) {
            $(".item-"+index).val(value);

            if(index == "rol")
            {
                console.log("ok");
                $( ".item-rol option:selected" ).text();
            }
         })
    });

    $("#form-user-edit").on('submit',function(e){
        e.preventDefault();
        var data = document.forms.namedItem("form-edit-user");
        var formData = new FormData(data);
        clearSpanFormUser();
        activar();
        senData("edit-user","modal-edit-users",formData);
    });

    

    $("#table_users tbody").on('click','.change-status',function(){
        var data = new FormData;
        data.append('id',$(this).data('id'));
        senData('change-status','sin-modal',data);
    });

    function senData(route,modal,data)
    {
        var modalJ = modal;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            console.log(data);
            if (data.success == "error")
            {
                modalEvent(data.success,data.msg);
                
               return true;
            }else{
                modalEvent(data.success,data.msg);
            }
            $('#' + modal + '').modal('hide');
            desactivar();
            $("#table_users").DataTable().ajax.reload();
         },
         error: function (errors, xhr, status,) {
            desactivar();
            var indices = errors.responseJSON.errors;
            $.each(indices, function (index, value) {
               console.log(index,value);
               $(".item-errors-" + index).append(value);
               $("-item-errors-" + index).show();
            });  
         }
    })
    }

    function clearSpanFormUser()
    {
        $(".item-errors-name").html("");
        $(".item-errors-email").html("");
        $(".item-errors-password").html("");
        $(".item-errors-rol").html("");
    }
    
    function desactivar() {
        $(".btnFetch").prop("disabled", false);
        
        $(".btnFetch").html(
            '<span></span> Guardar'
        );
     }
     
     function activar() {
        $(".btnFetch").prop("disabled", true);
        
        $(".btnFetch").html(
            `<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...`
        );
     }

});