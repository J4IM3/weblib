<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosUpdateTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos_update_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_tl',30);
            $table->string('codigo_evi',30);
            $table->longText('descripcion_alterna');
            $table->string('departamento_id')->nullable();
            $table->string('unidad_id')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos_update_temps');
    }
}
