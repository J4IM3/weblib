<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticulosVirtuales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos_virtuales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('itemCode',100);
            $table->longText('itemName');
            $table->longText('author');
            $table->float('price');
            $table->string('ean',30);
            $table->string('edition',100);
            $table->string('ubication',100);
            $table->string('isbn',30);
            $table->integer('onHand');
            $table->string('editorial');
            $table->string('editorial_name',200);
            $table->integer('type')->default(100);
            $table->float('matter');
            $table->integer('status')->default(1);
            $table->integer('categoria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos_virtuales');
    }
}
