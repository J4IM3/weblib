<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePapeleriaArticulosTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('papeleria_articulos_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_evi',30);
            $table->longText('descripcion');
            $table->longText('descripcion_alterna');
            $table->double('departamento_id');
            $table->string('proveedor',100);
            $table->string('unidad_id',50);
            $table->string('fecha_alta');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('papeleria_articulos_temps');
    }
}
