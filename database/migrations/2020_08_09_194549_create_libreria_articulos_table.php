<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibreriaArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libreria_articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('description');
            $table->string('autor')->nullable();
            $table->double('price');
            $table->string('ean')->nullable();
            $table->string('isbn')->nullable();
            $table->string('isbn_clean')->nullable();
            $table->float('stock');
            $table->integer('editorial_id');
            $table->string('type');
            $table->string('matter_id');
            $table->integer('bisac');
            $table->string('thema_categorie')->nullable();
            $table->string('code_sat');
            $table->date('high_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libreria_articulos');
    }
}
