<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListBisacCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_bisac_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('list');
            $table->integer('frontend');
            $table->integer('export');
            $table->string('code');
            $table->string('eng');
            $table->string('esp');
            $table->string('version');
            $table->string('sortOrder');
            $table->string('produkttypnavigation');
            $table->string('visibility');
            $table->string('hierarchie');
            $table->string('liste');
            $table->string('anmerkung');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_bisac_codes');
    }
}
