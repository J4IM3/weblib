<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('level1');
            $table->string('level2');
            $table->string('description_matter');
            $table->string('note');
            $table->string('level3');
            $table->string('level4');
            $table->string('level5');
            $table->string('level6');
            $table->string('level7');
            $table->string('level8');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themas');
    }
}
