<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypeArticleToResenias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resenias', function (Blueprint $table) {
            $table->string('type_article','3')->default('lib');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resenias', function (Blueprint $table) {
            $table->string('type_article','3')->default('lib');
        });
    }
}
