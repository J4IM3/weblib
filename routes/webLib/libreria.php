<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-08-09
 * Time: 02:18
 */
Route::GET("libreria",'LibreriaController@index')->name('libreria');
Route::POST("buscar-libro",'LibreriaController@find')->name('find-book');
Route::GET("buscar-por-editorial/{editorial}",'LibreriaController@findByEditorial')->name('find-by-editorials');
Route::GET("buscar-por-materia/{materia}",'LibreriaController@findBySection')->name('find-by-section');
Route::GET("buscar-por-proveedor/{proveedor}",'LibreriaController@findByProveedor')->name('find-by-proveedor');
Route::GET("libro/{code}",'LibreriaController@viewDetail')->name('view-details-lib');
Route::POST("actualizar-libro",'LibreriaController@update')->name('form-update-book-detail');
Route::POST("search-book",'LibreriaController@search')->name('search-book');