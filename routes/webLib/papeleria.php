<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-07-30
 * Time: 01:09
 */
    Route::GET('papeleria','PapeleriaController@listadoTl')->name('papeleria');
    Route::GET("get-ajax-pape",'PapeleriaController@getPapeAjax')->name('get-ajax-pape');


    Route::GET("detail-article/{id}",'PapeleriaController@detailArticle')->name('detail-article');
    Route::POST("buscar-articulo-papeleria",'PapeleriaController@findArticle')->name('find-article-pape');
    Route::GET("detalles-articulo/{code}",'PapeleriaController@viewDetail')->name('view-details');
    Route::GET("detalles-articulo-public/{code}",'PapeleriaController@viewDetailPublic')->name('view-details-public');
    Route::POST("add-image/{code}",'PapeleriaController@addImage')->name('add-image');

    Route::GET('form-search-pape','PapeleriaController@showFormSearch')->name('form-search-pape');
    Route::POST('search-article-pape-cte','PapeleriaController@formSearchArtPape')->name('search-article-pape-cte');
    Route::get('find-article-by-proveedor/{proveedor}','PapeleriaController@findArticleByProveedor')->name('find-article-by-proveedor');
    Route::get('get-article-by-matter/{matter}','PapeleriaController@findArticleByMatter')->name('get-article-by-matter');

//Listado p/tienda en linea
    Route::GET('get-table-pape-ajax','PapeleriaController@getArticle')->name('get-table-pape-ajax');
    Route::POST('get-info-by-code','PapeleriaController@getInfoByCode')->name('get-info-by-code');
    Route::POST('add-article-pape','PapeleriaController@addArticleTl')->name('add-article-pape');
    Route::POST('change-status-tl','PapeleriaController@changeStatusTl')->name('change-status-tl');
    Route::POST('update-article-pape','PapeleriaController@update')->name('update-article-pape');


/*
Route::POST('get-information.article','PapeleriaController@getDetailArticle')->name('get-information.article');
Route::POST('update-article-pape','PapeleriaController@update')->name('update-article-pape');
Route::POST('create-article-pape','PapeleriaController@create')->name('create-article-pape');
Route::GET('update-code-tl','PapeleriaController@updateCode')->name('update-code-tl');
*/
