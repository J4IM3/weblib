<?php
    Route::GET('users','UsersController@index')->name('users');
    Route::GET('get-users-ajax','UsersController@getUsersAjax')->name('get-users-ajax');
    Route::POST('new-user','UsersController@create')->name('new-user');
    Route::POST('edit-user','UsersController@edit')->name('edit-user');
    Route::POST('change-status','UsersController@changeStatus')->name('change-status');
?>
