<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function() {
    Auth::routes();
    Route::get('/', 'HomeController@index')->name('home');
    @include_once "webLib/libreria.php";
    @include_once "webLib/papeleria.php";
    @include_once "webLib/users.php";
    @include_once "webLib/config.php";
    @include_once "webLib/articulos_temporales.php";
    @include_once "webLib/articulos_virtuales.php";


    /**
     * todo :Ordenar estas rutas
     */
    Route::get('tl-libreria','ProductosController@index')->name('tl-libreria');
    Route::get('productos','ProductosController@productos')->name('productos');
    Route::get('guardar','ProductosController@guardar')->name('guardar');
    Route::get('validate','ProductosController@validarIsbn')->name('validate');
    Route::get('get-data','ProductosController@getData')->name('get-data');
    Route::get('save-data','ProductosController@save')->name('save-data');
    Route::get('buscar','ProductosController@buscar')->name('buscar');
    Route::get( 'libro.edit/{id}','ProductosController@edit')->name('libro.edit');
    Route::get('get-table-books-ajax','ProductosController@getDataBooks')->name('get-table-books-ajax');
    Route::POST('new-sinopsis','ProductosController@new')->name('new-sinopsis');
    Route::post('edit-sinopsis','ProductosController@editSinopsis')->name('edit-sinopsis');
    Route::get('actualizar','ProductosController@actualizar')->name('actualizar');
    Route::get('find-resenia','ProductosController@findResenia')->name('find-resenia');
    Route::get('libros-mexico','ProductosController@findLibrosMexico')->name('libros-mexico');

    Route::get('validar','ProductosController@validar')->name('validar');
    Route::get('get-sinopsis','ProductosController@getSinopsis')->name('get-sinopsis');


});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
