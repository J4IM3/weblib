<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUsersFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | max:250',
            'email' => 'required | email | unique:users,email,'.$this->id,
            'password' => 'required | min:5 '
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "El campo nombre es requerido",
            'name.max' => "El campo nombre debe ser minimo de 250 caracteres",
            'email.required' => "El correo es requerido",
            'email.email' => "El correo debe ser tipo email",
            'email.unique' => "Este correo ya se encuentra en uso",
            'password.requerid' => "Este campo es obligatorio",
            'password.min' => "La contraseña debe ser mayor a 5 caracteres"
        ];
    }
}
