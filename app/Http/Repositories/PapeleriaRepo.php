<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/22/19
 * Time: 10:40 PM
 */
namespace App\Http\Repositories;
use App\Http\Entities\ArticulosImages;
use App\Http\Entities\LibreriaArticulos;
use App\Http\Entities\Papeleria_articulos;
use App\Http\Entities\Proveedores;
use App\Http\Services\ChangeStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class PapeleriaRepo extends BaseRepo
{

    /**
     * @var ChangeStatus
     */
    private $changeStatus;

    public function getModel()
    {
        return new Papeleria_articulos();
    }

    public function __construct(ChangeStatus $changeStatus)
    {

        $this->changeStatus = $changeStatus;
    }

    public function find($data)
    {
        $option = $data['option-select-choise'];
        if ($option == "description")
        {
            return $this->findByDescription($data['article-search']);
        }

        if ($option == "proveedor")
        {
            return $this->findByProveedor($data['article-search']);
        }
        return $this->findArticle($option,$data['article-search']);
    }

    public function findArticle($option,$article_to_search)
    {
        return Papeleria_articulos::with(array('seccion_basic','precios','proveedores','unidadesPrecio','suppliers','mainUnit','department','artImagesUnique','artUnits' => function($query){
            $query->where('oumType','s')
                ->join('units', 'oumEntry', '=', 'units.uomEntry');
        }))->where($option,$article_to_search)->paginate(15);
    }

    public function findArticleByCode($code,$article_to_search)
    {
        $article = Papeleria_articulos::where('codigo_evi',$article_to_search)->first();
        if (!empty($article))
        {
            return  $article;
        }
        return ['success'=>'error','msg'=>'No se encontro el articulo'];
        /*return Papeleria_articulos::with(array('seccion_basic','proveedores','unidadesPrecio','suppliers','mainUnit','department','artImagesUnique','artUnits' => function($query){
            $query->where('oumType','s')
                ->join('units', 'oumEntry', '=', 'units.uomEntry')
            ;
        }))->where($code,$article_to_search)->get();*/
    }

    public function findByProveedor($proveedor)
    {

        $codeProveedor = Proveedores::where('cardName','like','%'.$proveedor.'%')->first();
        if (!empty($codeProveedor))
        {
            return Papeleria_articulos::where('proveedor',$codeProveedor->cardCode)->paginate(15);
        }
        Session::flash('alert-danger', 'Proveedor no encontrado, intenta una nueva busqueda');
        return redirect()->back();
    }

    public function findByDescription($description)
    {
        $texto[] = '';
        $texto = explode(" ", $description);
        $aleatorio = explode(" ", $description);
        $description = explode(" ", $description);
        $reverse = array_reverse($texto);
        shuffle($aleatorio);

        foreach ($reverse as $text) {
            $fullTextReverse[] = $text .'%';
        }

        foreach ($description as $text) {
            $fullDesription[] = $text .'%';
        }

        foreach ($aleatorio as $text) {
            $fullTextAleatorio[] = $text .'%';
        }

        $fullTextReverse = implode("", $fullTextReverse);
        $fullTextAleatorio = implode("", $fullTextAleatorio);
        $fullTextDesription = implode("", $fullDesription);

        $result = Papeleria_articulos::with('seccion_basic','department','artImagesUnique')->where('description','like','%'.$fullTextDesription.'%')->paginate(15);
        //DB::enableQueryLog(); $quries = DB::getQueryLog(); dd($quries);
        if (count($result) == 0){
            return Papeleria_articulos::with('seccion_basic','department','artImagesUnique')->where('description','like','%'.$fullTextReverse."%")->paginate(15);
            if (count($result == 0))
            {
                return Papeleria_articulos::with('seccion_basic','department','artImagesUnique')->where('description','like','%'.$fullTextAleatorio.'%')->paginate(15);
            }
        }
        return $result;

    }

    public function addImage($code,$request)
    {
        $image = $request->file('myImage');
        $name = $code.date('his').".".$request->file('myImage')->getClientOriginalExtension();
        $image->move('images/papeleria/', $name);
        ArticulosImages::create([
            'article_code' => $code,
            'url_image' => "images/papeleria/".$name
        ]);
        Session::flash('alert-success', 'Imagen agregada correctamente');
        return redirect()->back();
    }

    public function addArticleTl($data)
    {
        $codetl = Papeleria_articulos::orderBy('code_tl_numeric','DESC')->first();

        $article = Papeleria_articulos::where('code',$data['code'])->first();
        $article->description_tl = $data['descripcion_tl'];
        $article->unidad_tl = $data['unidad_tl'];
        $article->department_tl = $data['departamento_id'];
        $article->activate_tl = 1;
        if (empty($article->code_tl))
        {
            $newCode = $codetl->code_tl_numeric +1;
            $article->code_tl = "P00".$newCode;
            $article->code_tl_numeric = $newCode;
        }
        if ($article->save())
        {
            return ['success' =>'success','msg'=>'Registro actualizado'];
        }
        return ['success' =>'error','msg'=>'Ocurrio un error'];
    }

    public function changeStatusTl($code)
    {
        $article = Papeleria_articulos::where('codigo_evi',$code)->first();
        $article->status = $this->changeStatus->changeStatus($article->status);
        if ($article->save())
        {
            return ["success" => 'success','msg'=>'Artículo desactivado'];
        }
    }

    public function sendDataFind($data)
    {
        foreach ($data['select-option-pape'] as $key => $item)
        {
            $data['option-select-choise'] = $item;

        }
        foreach ($data['searchPape']as $key => $item)
        {
            $data['article-search'] = $item;
        }
        return $this->find($data);
    }

    public function sendDataMultiple($data)
    {
        $values = $data['searchPape'];
        $item = [];
        $sql1 ="";
        $sql ="";
        $position = 0;
        $position1 = 0;
        $operador="";
        foreach ($data['select-option-pape'] as $key)
        {
            $item[$key] = $values[$position];
            $position = $position + 1;
        }

        foreach ($item as $key => $value)
        {
            $fullText[] ="";
            $sql ="";
            $texto = explode(" ", $value);

            foreach ($texto as $tex)
            {
                $fullText[] = $tex .'%';
            }
            $txt = implode("", $fullText);
            if ($position > 0 and $position1<$position-1)
            {
                $operador ="or";
                $like = "%";
            }else{
                $operador ="";
            }
            $sql = $key." like '$like$txt' $operador ";
            $sql1 = $sql1.$sql;
            $sql= "";
            unset($sql);
            unset($fullText);
            $position1 = $position1 + 1;

        }
        $query = "select code from papeleria_articulos where  $sql1";
        $result = DB::select($query);
        $code = [];
        foreach ($result as $item)
        {
            $code[] = $item->code;
        }

        return Papeleria_articulos::with('seccion_basic','department','artImagesUnique','precios','unidadesPrecio')->whereIn('code',$code)->get();
    }

}
