<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/22/19
 * Time: 10:40 PM
 */
namespace App\Http\Repositories;
use App\Http\Entities\Sinopsi;
use App\Http\Repositories\BaseRepo;
class SinopsisRepo extends BaseRepo{

    public function getModel()
    {
        return new Sinopsi();
    }
}
