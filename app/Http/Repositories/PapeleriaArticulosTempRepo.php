<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/22/19
 * Time: 10:40 PM
 */
namespace App\Http\Repositories;
use App\Http\Entities\Papeleria_articulos;
use App\Http\Repositories\BaseRepo;
use App\PapeleriaArticulosTemp;
use http\Env\Request;

class PapeleriaArticulosTempRepo extends BaseRepo
{

    public function getModel()
    {
        return new Papeleria_articulos();
    }

    public function insert()
    {
        $articulosTemp = PapeleriaArticulosTemp::all();
        $importados = 0;
        $rechazados = 0;
        foreach ($articulosTemp as $articuloTemp)
        {
            $articulo = Papeleria_articulos::where('codigo_evi',$articuloTemp->codigo_evi)
                ->where('unidad_id',$articuloTemp->unidad_id)
                ->first();

            $unidad = $this->getUnidad($articuloTemp);

            if (empty($articulo))
            {
                $codigTL = $this->codigoTL();
                $articuloTemp = Papeleria_articulos::create([
                    'codigo_evi' => $articuloTemp->codigo_evi,
                    'codigo_tl' => $codigTL,
                    'descripcion' => $articuloTemp->descripcion,
                    'descripcion_alterna' => $articuloTemp->descripcion_alterna,
                    'proveedor' => $articuloTemp->proveedor,
                    'unidad_id' => $unidad,
                    'departamento_id' => $articuloTemp->departamento_id,
                ]);
                $importados = $importados +1;
                $articuloTemp->status = 2;
                $articuloTemp->save();
            }else{
                $rechazados = $rechazados +1;
                $articuloTemp->status = 3;
                $articuloTemp->save();
            }
        }
        echo "Importados: ".$importados."<br>";
        echo "Rechazados: ".$rechazados."<br>";

    }

    public function codigoTL()
    {
        $articulos = Papeleria_articulos::all();
        $ultimoArticulo = $articulos->last();
        $ultimoCodigo = substr($ultimoArticulo->codigo_tl, 3) + 1;
        return "P00".$ultimoCodigo;
    }

    public function getUnidad($articulo)
    {
        if (isset($articulo->unidad_id))
        {
            return "";
        }
        return "'".$articulo->unidad_id."'";
    }

}
