<?php

namespace App\Http\Repositories;

use App\Http\Services\ChangeStatus;
use App\User;
use Illuminate\Support\Facades\Hash;
class UsersRepo extends BaseRepo
{
    private $changeStatus;
    public function getModel()
    {
        return new User();
    }

    public function __construct(ChangeStatus $changeStatus)
    {
        $this->changeStatus = $changeStatus;       
    }

    public function create($data)
    {
        $user  =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'rol' => $data['rol'],
            'password' => Hash::make($data['password']),
        ]);

        if(!empty($user))
        {
            return ['success'=>'success','msg'=>'Usuario creado correctamente'];
        }
    }

    public function edit($data)
    {
        $user = User::where('id',$data['id'])->first();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->rol = $data['rol'];
        if($user->save())
        {
            return ["success" => "success","msg" => "Registro actualizado"];
        }
    }

    public function changeStatus($id)
    {
        $user = User::where('id',$id)->first();
        $user->status = $this->changeStatus->changeStatus($user->status);
        if($user->save())
        {
            return ["success" => "success","msg" => "Registro actualizado"];
        }
    }
}