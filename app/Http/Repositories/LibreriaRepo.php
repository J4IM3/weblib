<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-08-14
 * Time: 05:57
 */

namespace App\Http\Repositories;


use App\Http\Entities\Editorials;
use App\Http\Entities\LibreriaArticulos;
use App\Http\Entities\Proveedores;
use App\Http\Entities\Resenias;
use http\Env\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Monolog\Handler\IFTTTHandler;

class LibreriaRepo extends BaseRepo
{

    public function getModel()
    {
        return new LibreriaArticulos();
    }

    public function findBook($data)
    {
        $option = $data['option-select-choise'];
        if ($data['option-select-choise'] == "description" or $data['option-select-choise'] == "autor"  )
        {
            return $this->findBookOption($option,$data['article-search']);
        }
        if ($data['option-select-choise'] == "editorial" )
        {
            $code_editorial = Editorials::select('code')->where('name',$data['article-search'])->first();
            return $this->findByEditorial($code_editorial->code);
        }

        return $this->codeOrIsbn($data['option-select-choise'],$data['article-search']);

    }

    public function codeOrIsbn($option,$value)
    {
        //DB::enableQueryLog();
        return $query = LibreriaArticulos::with('proveedoresLib','sectionsLib','artImagesLib','editorials','sinopsis','existenciasLib')->where($option,$value)->paginate(15);
        //$quries = DB::getQueryLog();
        //dd($quries);
    }

    public function findBookOption($option,$value)
    {
        $texto = explode(" ", $value);
        $aleatorio = explode(" ", $value);
        $value = explode(" ", $value);
        $reverse = array_reverse($texto);
        shuffle($aleatorio);

        foreach ($reverse as $text) {
            $fullTextReverse[] = $text .'%';
        }

        foreach ($value as $text) {
            $fullDesription[] = $text .'%';
        }

        foreach ($aleatorio as $text) {
            $fullTextAleatorio[] = $text .'%';
        }
        $fullTextDesription = implode("", $fullDesription);
        $fullTextReverse = implode("", $fullTextReverse);
        $fullTextAleatorio = implode("", $fullTextAleatorio);

        //DB::enableQueryLog();
        $results = LibreriaArticulos::where($option,'like','%'.$fullTextDesription)->paginate(15);
        //$quries = DB::getQueryLog(); dd($quries);

        if (count($results) == 0)
        {

            $results = LibreriaArticulos::where($option,'like','%'.$fullTextReverse)->paginate(15);
            if (count($results) == 0)
            {
                $results = LibreriaArticulos::where($option,'like','%'.$fullTextAleatorio)->paginate(15);
            }
        }
        return $results;
    }

    public function findByEditorial($editorial)
    {
        return LibreriaArticulos::with('sectionsLib','artImagesLib','editorials')->where('editorial_id',$editorial)->paginate(15);
    }

    public function findBySection($materia)
    {
        return LibreriaArticulos::with('sectionsLib','artImagesLib','editorials','')->where('matter_id',$materia)->paginate(15);
    }

    public function update($data)
    {
        $book = LibreriaArticulos::where('code',$data['code'])->first();
        $book->bisac = $data['bisac'];
        $book->thema_categorie = $data['thema_categorie'];
        $book->matter_id = $data['matter_id'];
        if ($book->save())
        {
                return $this->saveSynopsisBook($book,$data['sinopsis']);
        }
    }

    public function saveSynopsisBook($book,$sinopsis)
    {
        $synopsisBook = Resenias::where('codigo',$book['code'])->first();
        if (!empty($synopsisBook))
        {
            $synopsisBook->sinopsis = $sinopsis;
            if ($synopsisBook->save())
            {
                Session::flash('alert-success', 'Articulo actualizado');
                return redirect()->back();
            }
            Session::flash('alert-danger', 'Ocurrio un error');
            return redirect()->back();
        }
        Resenias::create([
            'isbn' => $book->isbn,
            'sinopsis' => $sinopsis ,
            'codigo' => $book->code,
            'titulo' => $book->description,
            'isbn2' => $this->convertirIsbn($book->isbn),
            'status'=> 1
        ]);
        Session::flash('alert-success', 'Articulo actualizado');
        return redirect()->back();
    }

    public function convertirIsbn($item)
    {
        $isbn = str_replace("-", "", $item);
        if (strlen($isbn) != 10) {
            return $isbn;
        }
    }

    public function searchMultiple($data)
    {
        $values = $data['article-search'];
        $item = [];
        $sql1 ="";
        $sql ="";
        $position = 0;
        $position1 = 0;
        $operador="";

        foreach ($data['option-select-choise'] as $key => $v)
        {
            if ($v == 'editorial_id')
            {
                $editorial = $data['article-search'][$position];
            }
            $position = $position+1;
        }
        $editoria_id = $this->findEditorialByName($editorial);

        foreach ($data['description'] as $key => $value)
        {
            $fullText[] ="";
            $sql ="";
            $texto = explode(" ", $value);

            foreach ($texto as $tex)
            {
                $fullText[] = $tex .'%';
            }
            $txt = implode("", $fullText);
            if ($position > 0 and $position1<$position-1)
            {
                $operador ="or";
                $like = "%";
            }else{
                $operador ="";
            }
            $sql = $key." like '$like$txt' $operador ";
            $sql1 = $sql1.$sql;
            $sql= "";
            unset($sql);
            unset($fullText);
            $position1 = $position1 + 1;

        }

        $query = "select code from libreria_articulos where  $sql1";
        $result = DB::select($query);
        $code = [];
    }

    public function findEditorialByName($editorial)
    {
        return Editorials::select('code')->where('name','like','%'.$editorial.'%')->first();
    }


}
