<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/22/19
 * Time: 10:40 PM
 */
namespace App\Http\Repositories;
use App\Http\Entities\Papeleria_articulos;
use App\Http\Entities\Resenias;
use App\Http\Repositories\BaseRepo;
use http\Env\Request;

class PapeleriaArticulosRepo extends BaseRepo
{

    public function getModel()
    {
        return new Papeleria_articulos();
    }

    public function create($data)
    {
        $articulos= Papeleria_articulos::all();
        $last = $articulos->last();
        $ultimo = $last->code_tl_extension +1;
        $articulo = Papeleria_articulos::where('codigo_evi',$data['codigo_evi'])->first();
        if (!empty($articulo))
        {
            return ['success' => 'error','msg' =>'Este articulo ya existe'];
        }


        Papeleria_articulos::create([
           'codigo_evi' => $data['codigo_evi'],
           'codigo_tl' => "P00".$ultimo,
           'code_tl_extension' => $ultimo,
           'descripcion' => $data['descripcion'],
           'descripcion_alterna' => $data['descripcion_alterna'],
           'proveedor' => $data['proveedor'],
           'unidad_id' => "'".$data['unidad_id']."'",
           'departamento_id' => $data['departamento_id'],
        ]);
        return ['success' => 'success','msg' =>'Artículo agregado'];
    }

    public function update($data)
    {

        //{code: "B-L-DUO", descripcion_tl: "CAJA CON 12 PIEZAS DE LAPIZ DUO VERITHIN GRAFITO Y COLOR ROJO MARCA PAPER MATE", unidad_tl: "caja12", departamento_id: "0"}
        if (empty($data['unidad_tl']))
        {
            $data['unidad_tl'] ="";
        }else{
            $data['unidad_tl'] ="'".$data['unidad_tl']."'";
        }
        $articulo = Papeleria_articulos::where('codigo_evi',$data['code'])->first();
        $articulo->codigo_evi = $data['code'];
        $articulo->descripcion_alterna = $data['descripcion_tl'];
        $articulo->unidad_id = $data['unidad_tl'];
        $articulo->departamento_id = $data['departamento_id'];
        if ($articulo->save())
        {
            if (!empty($data['descripcion']))
            {
                $this->description($articulo->codigo_evi,$data['descripcion'],$articulo->codigo_tl,'pap',$articulo->descripcion_alterna);
            }
            return ['success' => 'success','msg' => "Registro actualizado."];
        }

    }

    public function description($itemCode,$descripcion,$codigoTl,$tipo,$titulo){
        $resenia = Resenias::where('codigo',$codigoTl)->first();
        if (!isset($resenia)){
            Resenias::create([
                'isbn' => $itemCode,
                'sinopsis' => $descripcion,
                'codigo' => $codigoTl,
                'titulo' => $titulo,
                'isbn2' => $itemCode,
                'status' => 1,
                'type_article' => $tipo
            ]);
            return "true";
        }

        $resenia->sinopsis = $descripcion;
         if ($resenia->save()){
             return true;
         }
    }

}
