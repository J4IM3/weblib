<?php

namespace App\Http\Controllers;

use App\Http\Entities\ArticulosImages;
use App\Http\Entities\Code_departments;
use App\Http\Entities\Codedepartments;
use App\Http\Entities\Departamentos;
use App\Http\Entities\Papeleria_articulos;
use App\Http\Entities\PrecioUnidades;
use App\Http\Entities\Proveedores;
use App\Http\Entities\Section_basics;
use App\Http\Repositories\PapeleriaArticulosRepo;
use App\Http\Repositories\PapeleriaRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class PapeleriaController extends Controller
{
    /**
     * @var PapeleriaArticulosRepo
     */
    private $papeleriaArticulosRepo;
    /**
     * @var PapeleriaRepo
     */
    private $papeleriaRepo;

    public function __construct(PapeleriaArticulosRepo $papeleriaArticulosRepo,PapeleriaRepo $papeleriaRepo)
    {
        $this->papeleriaArticulosRepo = $papeleriaArticulosRepo;
        $this->papeleriaRepo = $papeleriaRepo;
    }

    public function index()
    {
        $results = "";
        return view('papeleria.index',compact('results'));
    }

    public function getPapeAjax()
    {
        $articles = Papeleria_articulos::with('seccion_basic','department')->limit(20)->get(); ;
        return DataTables::of($articles)->make(true);
    }

    public function detailArticle($id)
    {
        $article = Papeleria_articulos::with('seccion_basic','department','artImage')->where('id',$id)->first();
        $sections = Section_basics::all();
        return view('papeleria.detail',compact('article','sections'));
    }

    public function findArticle(Request $request)
    {
        $results = $this->papeleriaRepo->find($request->all());
        return view('papeleria.index',compact('results'));
    }

    public function viewDetail($code)
    {
        $data = [];
        $uniPrecio =[];
        $article = Papeleria_articulos::with('department','seccion_basic','unidadesPrecio','artImages','precios','unidadesPrecio','existencias')->where('code',$code)->first();
        $unidadesPrecio = PrecioUnidades::select('UomName')
            ->where('ItemCode',$code)->distinct()->get();

            foreach($unidadesPrecio as $up)
            {
                $preciosU = PrecioUnidades::select('mayoreo','menudeo')->where('UomName',$up->UomName)->where('ItemCode',$code)->get();
                foreach($preciosU as $value)
                {

                    if($value->menudeo == null)
                    {
                        $unidadesM = $value->mayoreo;
                        $data[$up->UomName]['mayoreo'] = $unidadesM;
                    }else{
                        $unidadesMe = $value->menudeo;
                        $data[$up->UomName]['menudeo'] = $unidadesMe;
                    }
                    unset($unidadesM);
                    unset($unidadesMe);
                }
            }
            $article = Arr::add($article, 'unidadesP', $data);
        return view('papeleria.detail',compact('article'));

    }

    public function addImage($code,Request $request)
    {
        return $this->papeleriaRepo->addImage($code,$request);
    }

    public function listadoTl()
    {
        $departments = Departamentos::where('status',1)->get();
        return view('papeleria.listado',compact('departments'));
    }

    public function getInfoByCode(Request $request)
    {
        return $this->papeleriaRepo->findArticleByCode("codigo_evi",$request->get('code'));
    }



    public function getArticle()
    {
        $articulos = Papeleria_articulos::with('department','reseniasPape')->get();
        foreach ($articulos as $article)
        {
            $str = str_replace(array('\'', '"'), '', $article->unidad_id);
            $article->unidad_id = $str;
        }
        return DataTables::of($articulos)->make('true');
    }

    public function getDetailArticle(Request $request)
    {
        return Papeleria_articulos::with('seccion_basic','suppliers','mainUnit','department','artImagesUnique')->where('codigo_evi', $request->get('codigo'))->first();
    }

    public function addArticleTl(Request $request)
    {
        return $this->papeleriaRepo->addArticleTl($request->all());
    }

    public function changeStatusTl(Request $request)
    {
        return $this->papeleriaRepo->changeStatusTl($request->get('code'));
    }

    public function update(Request $request)
    {
        return $this->papeleriaArticulosRepo->update($request->all());
    }

    public function updateCode()
    {
        $articulos = Papeleria_articulos::all();
        foreach ($articulos as  $articulo)
        {
            $codigo = substr($articulo->codigo_tl, -5, 8);
            $articulo->code_tl_extension = $codigo;
            $articulo->save();
        }
    }
    public function showFormSearch(){
        return view('papeleria.buscar');
    }

    public function formSearchArtPape(Request $request)
    {
        $queryDescription = "";
        $queryProveedor = "";
        $flag = 0;
        $operator = "";
        $code = [];
        if($request->get('description') != null)
        {
            $descripcion = $request->get('description');
            $queryDescription = "description like '%$descripcion%'";
            $flag = 1;
        }
        if($request->get('proveedor') != null)
        {
            $proveedor = $request->get('proveedor');
            $codeProveedor = Proveedores::select('cardCode')->where('cardName','like','%'.$proveedor.'%')->first();
            if($flag == 1)
            {
                $operator = "and";
            }
            $queryProveedor = $operator." proveedor  like '%$codeProveedor->cardCode%'";
        }


        $sql = "select code from papeleria_articulos where ".$queryDescription.$queryProveedor;
        $result = DB::select($sql);

        foreach($result as $res)
        {
            $code[] = $res->code;
        }

        $results = Papeleria_articulos::with('department','seccion_basic','unidadesPrecio','artImages','precios','unidadesPrecio','existencias')->whereIn('code',$code)->paginate(15);

        return view('papeleria.index',compact('results'));
    }

    public function viewDetailPublic($code)
    {
        $article = Papeleria_articulos::with('department','seccion_basic','artImages','precios','unidadesPrecio','existencias')->where('code',$code)->first();
        return view('papeleria.detail_public',compact('article'));
    }

    public function findArticleByProveedor($proveedor)
    {
        $results = Papeleria_articulos::where('proveedor',$proveedor)->paginate(15);
        return view('papeleria.index',compact('results'));
    }

    public function findArticleByMatter($matter)
    {
        $results = $article = Papeleria_articulos::with('department','seccion_basic','artImages','precios','unidadesPrecio','existencias')->where('section',$matter)->paginate(15);
        return view('papeleria.index',compact('results'));
    }


}


