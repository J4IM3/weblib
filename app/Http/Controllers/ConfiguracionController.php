<?php

namespace App\Http\Controllers;

use App\Http\Entities\ArticulosImages;
use App\Http\Entities\Papeleria_articulos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ConfiguracionController extends Controller
{
    public function index()
    {
        return view('config.index');
    }

    public function findImages()
    {
        $artImagenes = ArticulosImages::all();
        $artIma = [];
        foreach($artImagenes as $art)
        {
            $artIma[] = $art->article_code;
        }
        $articulosPapeleria = Papeleria_articulos::whereNotIn('code',$artIma)->get();
        $contador = 0;
        foreach($articulosPapeleria as $articulo)
        {
            $dirpng = glob('images/productos_img/' . $articulo->codeBars . '.png');
            $dirjpg = glob('images/productos_img/' . $articulo->codeBars . '.jpg');
            //$dirpng = glob('images/productos_img/' . $articulo->itemCode . '.png');
            
            if (count($dirpng )!= 0 )
            {
                $this->addImage($dirpng[0],$articulo->code);
                $contador = $contador +1;
            }elseif(count($dirjpg )!= 0 ){
                $this->addImage($dirjpg[0],$articulo->code);
                $contador = $contador +1;
            }
        }
        Session::flash('alert-success', 'Se agregaron '.$contador.' imagenes');
        return back();

    }

    public function addImage($ruta,$data)
    {
        ArticulosImages::create([
            'article_code' => $data,
            'url_image' => $ruta
        ]);
    }
}
