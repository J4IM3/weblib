<?php

namespace App\Http\Controllers;

use App\Http\Entities\Editorials;
use App\Http\Entities\LibrarySections;
use App\Http\Entities\LibreriaArticulos;
use App\Http\Entities\ListBisacCodes;
use App\Http\Entities\Themas;
use App\Http\Repositories\LibreriaRepo;
use App\Http\Repositories\PapeleriaRepo;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\DeclareDeclare;

class LibreriaController extends Controller
{
    /**
     * @var PapeleriaRepo
     */
    private $papeleriaRepo;
    /**
     * @var LibreriaRepo
     */
    private $libreriaRepo;

    public function __construct(PapeleriaRepo $papeleriaRepo,LibreriaRepo $libreriaRepo)
    {

        $this->papeleriaRepo = $papeleriaRepo;
        $this->libreriaRepo = $libreriaRepo;
    }
    public function index(){
        //$libros = $this->productosRepo->paginate(5);
        //$libros = Resenias::paginate(5);
        $results = "";
        return view('libreria.index',compact('results'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Busca un libro dependiendo la opcion enviada por el usuario.
     */
    public function find(Request $request)
    {
        $results = $this->libreriaRepo->findBook($request->all());
        return view('libreria.index',compact('results'));
    }

    /**
     * @param $editorial
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Busca libros por editorial
     */
    public function findByEditorial($editorial)
    {
        $results = $this->libreriaRepo->findByEditorial($editorial);
        //dd($results);
        return view('libreria.index',compact('results'));
    }


    /**
     * @param $materia
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Busca libros por materia
     */

    public function findBySection($materia)
    {
        $results = $this->libreriaRepo->findBySection($materia);
        return view('libreria.index',compact('results'));
    }

    /*
     * Nos retorna el detalle de un articulo de libreria
     */
    public function viewDetail($code)
    {
        $books = $this->libreriaRepo->codeOrIsbn("code",$code);
        $themas = Themas::all();
        $listBisac = ListBisacCodes::all();
        $librarySection = LibrarySections::all();
        return view('libreria.detail',compact('books','listBisac','themas','librarySection'));
    }

    /**
     * Se actualizan los datos del libro seleccionad
     */
    public function update(Request $request)
    {
        return $this->libreriaRepo->update($request->all());
    }

    public function search(Request $request)
    {
        $position = 0;
        $ql = "";
        $data = [];
        $query = [];
        if (!empty($request->get('editorial')))
        {
            $editorial_id = Editorials::select('code')->where('name','like','%'.$request->get('editorial'))->first();
            $data['editorial_id'] = "editorial_id = ".$editorial_id->code;
        }
        if (!empty($request->get('description')))
        {
            $titulo = $request->get('description');
            $data['description'] = " description like '%$titulo%'";
        }
        if (!empty($request->get('autor')))
        {
            $autor = strtoupper($request->get('autor'));
            $autor1 = explode(" ",$autor);
            foreach ($autor1 as $text) {
                $fullTextAutor[] = $text .'%';
            }
            $autor = implode("", $fullTextAutor);

            //$data['description'] = " description like '%autor%'";
            $data['autor'] = "autor like '%$autor'";

        }


        foreach ($data as  $key =>  $datum)
        {

            if ($position  > 0)
            {
                $query[] = ' and '.$datum;
            }else{
                $query[] = $datum;
            }
            $position = $position +1;
        }

        $datos = implode($query);
        $sql = "select * from libreria_articulos where ".$datos;
        $result = DB::select($sql);
        $code = [];
        foreach ($result as $item)
        {
            $code[] = $item->code;
        }
        $results = LibreriaArticulos::with('sectionsLib','artImagesLib','editorials')->whereIn('code',$code)->paginate(15);
        return view('libreria.index',compact('results'));

    }

    public function findByProveedor($proveedor)
    {

        $results = LibreriaArticulos::with('proveedoresLib','sectionsLib','artImagesLib','editorials','sinopsis','existenciasLib')->where('cardCode',$proveedor)->paginate(15);
        return view('libreria.index',compact('results'));
    }
}
