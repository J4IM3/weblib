<?php

namespace App\Http\Controllers;

use App\Http\Entities\ArticulosUpdateTemp;
use App\Http\Entities\Papeleria_articulos;
use Illuminate\Http\Request;

class ArticulosUpdateTempController extends Controller
{
    public function actualizarDescripcion()
    {
        $articulosTemps = ArticulosUpdateTemp::all();

        foreach ($articulosTemps as $articulosTemp)
        {
            $articulo = Papeleria_articulos::where('codigo_tl',$articulosTemp->codigo_tl)
                ->where('codigo_evi',$articulosTemp->codigo_evi)
                ->first();

            if (!empty($articulo))
            {
                $articulo->descripcion_alterna = $articulosTemp->descripcion_alterna;
                $articulo->save();

                $articulosTemp->status = 2;
                $articulosTemp->save();


                echo $articulo;
            }else{
                $articulosTemp->status = 3;
                $articulo->save();
            }
            $articulo->save();
        }
        echo "Finalizado";
    }
}
