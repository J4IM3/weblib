<?php

namespace App\Http\Controllers;

use App\Http\Entities\Papeleria_articulos;
use App\Http\Repositories\PapeleriaArticulosTempRepo;
use App\PapeleriaArticulosTemp;
use Illuminate\Http\Request;

class PapeleriaArticulosTempController extends Controller
{

    /**
     * @var PapeleriaArticulosTempRepo
     */
    private $papeleriaArticulosTempRepo;

    public function __construct(PapeleriaArticulosTempRepo $papeleriaArticulosTempRepo)
    {

        $this->papeleriaArticulosTempRepo = $papeleriaArticulosTempRepo;
    }

    public function insertar()
    {
        return $this->papeleriaArticulosTempRepo->insert();
    }


}
