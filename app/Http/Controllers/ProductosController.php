<?php

namespace App\Http\Controllers;

use App\Http\Entities\Codigos;
use App\Http\Entities\Resenias;
use App\Http\Repositories\ProductosRepo;
use App\Http\Repositories\SinopsisRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Image;
use Scriptotek\GoogleBooks\GoogleBooks;
use Yajra\DataTables\DataTables;
use Intervention\Image\ImageManager;


class ProductosController extends Controller
{
    /**
     * @var ProductosRepo
     */
    private $productosRepo;
    /**
     * @var SinopsisRepo
     */
    private $sinopsisRepo;
    /**
     * @var ReseniasRepo
     */
    private $reseniasRepo;

    public function __construct(ProductosRepo $productosRepo)
    {
        $this->productosRepo = $productosRepo;
    }

    public function index()
    {
        return view('index');
    }


    public function nuevo(){
        return view('productos');
    }

    public function guardar(Request $request){

        Resenias::create([
            'isbn'=>$request->get('isbn'),
            'sinopsis' => $request->get('sinopsis')
        ]);
        return redirect::back()->with('message', 'Resenias creada con exito');

    }



     public function validarIsbn () {
        //978-968-26-1037-0

        $isbn= Codigos::where('id','>',1)
            ->where('id','<',16505)
            ->get();
        foreach ($isbn as $item){
            $isbn = str_replace("-", "", $item->isbn);
             if ( strlen($isbn) != 10 ){
                 $item->status =1;
                 if ($item->isbn == "")
                 {
                     $item->status = 2;

                 }
                 $item->isbn2 = $isbn;
                 $item->save();
             }
         }


        $caracteres = $isbn.str_split('');

        if($caracteres[9].toUpperCase() == 'X'){
            $caracteres[9] = 10;
        }

        $suma = 0;

        for ($i = 0; $i < $caracteres.strlen(); $i++) {
            $suma += ((10-$i) * parseInt($caracteres[$i]));
            };

        return array( (($suma % 11) == 0));

        }

        public function getData(){
            return Codigos::where('id','>',5201)
                ->where('id','<',6202)
                ->where('status','1')
                ->get();
        }

        /*
        public function save($isbn,$description){
            $books = Resenias::create([
                'isbn'=>$isbn,
                'sinopsis'=>$description
            ]);
            return $books;
        }


        public function buscar(){
            $books = new GoogleBooks(['key' => 'AIzaSyDUEkA2S8x0elF3Tl3mtyxpeAt-b4ioc-k']);
            $isbns = $this->getData();
            foreach ($isbns as $isbn){
                $volume = $books->volumes->byIsbn($isbn->isbn);
                var_dump($volume);
                //$volume = $books->volumes->byIsbn("9789681659707");
                $result = json_decode($volume, true);
                if (!empty($result['volumeInfo']['description']))
                {
                    $this->save($isbn->isbn,$result['volumeInfo']['description']);
                }
            }
            return "acabo";
        }
        */
        public function edit(Request $request)
        {
            $libro =Resenias::where($request->all());
            return $libro;
        }

        public function getDataBooks(){
            $books = $this->productosRepo->all();
            return DataTables::of($books)->make(true);
        }

        public function new(Request $request){
            $libros = Resenias::where('codigo',$request->get('codigo'))->first();
            if (!empty($libros) )
            {
                return ['success' => 'error','msg' => 'Esta sinopsis ya existe, id:  '.$libros->id];
            }
            $isbn = $this->convertirIsbn($request->get('isbn'));
            $productos = Resenias::create([
                'isbn'=>$request->get('isbn'),
                'isbn2'=>$isbn,
                'sinopsis'=>$request->get('sinopsis'),
                'codigo'=>$request->get('codigo'),
                'titulo'=>$request->get('titulo'),
                'status'=>1

            ]);
            if (empty($productos)){
                return ['success'=>'error','msg'=>'Ah ocurrido un error , consulte a su administrador'];
            }
            return ['success'=>'success','msg'=>'Registro guardado'];

        }

        public function convertirIsbn($item)
        {
            $isbn = str_replace("-", "", $item);
            if (strlen($isbn) != 10) {
                return $isbn;
            }
        }

        public function editSinopsis(Request $request){
            $book = $this->productosRepo->find($request->get('id'));
            $book->isbn = $request->get('isbn');
            $book->sinopsis = $request->get('sinopsis');
            $book->codigo = $request->get('codigo');
            $book->titulo = $request->get('titulo');
            if (!$book->save()){
                return ['success'=>'error','msg'=>'Ah ocurrido un error , consulte a su administrador'];
            }
            return ['success'=>'success','msg'=>'Registro guardado'];

        }


        /**
         *
         */
        public function actualizar(){
            $codigos = Codigos::all();
            $resenias = Resenia::all();
            return $resenias;
        }

        public function findResenia(){
            $books = new GoogleBooks(['key' => 'AIzaSyDUEkA2S8x0elF3Tl3mtyxpeAt-b4ioc-k']);
            $isbn = Codigos::where('id','>',1107)
                ->where('id','<',1200)
                ->get();

            foreach ($isbn as $item){
                if ($item->isbn != null){
                    $volume = $books->volumes->byIsbn($item->isbn);
                    $result = json_decode($volume, true);
                }

                if (!empty($result['volumeInfo']['description']))
                {
                    $p = Resenias::create([
                        'isbn'=>$item->isbn,
                        'sinopsis'=>$result['volumeInfo']['description'],
                        'codigo'=>$item->itemcode,
                        'titulo'=>$item->itemname,
                        'isbn2'=>$item->isbn2,
                        'status'=>1
                    ]);
                    $item->status=2; //sinopsis encontrada
                }else{
                    $item->status = 3;//sinopsis no encontrada
                }

                $item->save();
            }

        }

        /**
         * Prueba appi libros mexico
         */

         public function findLibrosMexico(){
             $contador = 0;
             $portadas_null = [];
             $libros = Codigos::where('id','>',5000)->where('id','<',6000)->get();
             //$libros = Codigos::where('id','=',2187)->get();

             foreach ($libros as $key => $libro) {
                 if ($libro->isbn2 != null)
                 {
                     $id_libro_resenia = Resenias::where('isbn2','=',$libro->isbn2)->first();
                     if (empty($id_libro_resenia))
                     {
                         $data = $this->getDataFromLibrosMexico( $libro->isbn);
                         if ($data[0]['encabezado'][1]['codigo'] != 400)
                         {
                             $sinopsis = $this->saveSinopsis($data[0]['cuerpo']['sinopsis'],$libro);
                             if (!empty($data[0]['cuerpo']['img_grande']))
                             {
                                 $portada = $this->saveImage($data[0]['cuerpo']['img_grande'],$libro->isbn);
                                 $libro->status_imagen = 1;
                                 $libro->save();
                             }
                         }
                     }
                 }
            }

            return "termino ".$contador;
         }

         public function getDataFromLibrosMexico($isbn)
         {
            $fecha = date("YmdHis");
            $integrador = 17;
            $llave = "7fa587697d72392a0761d8084095adb4";
            $atributos = http_build_query(
                array(
                    'fecha' => $fecha,
                    'integrador' => $integrador,
                    'firma' => md5($fecha . $integrador . $llave),
                    'isbn' => $isbn,
                    'id_libro'=>1,
                    'orden' => 'fecha_colofon'
                )
            );

            $solicitud =
                array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $atributos
                )
            );

            $contexto = stream_context_create($solicitud);
            $resultado = file_get_contents( 'https://pro.librosmexico.mx/webservices/json2/getLibro', false, $contexto);
            $someArray = json_decode($resultado, true);
            return $someArray;
        }

        public function saveSinopsis($data,$book)
        {

            //; Aqui viene la sinopsis
            Resenias::create([
                'isbn' => $book->isbn,
                'sinopsis' => $data,
                'codigo' => $book->itemcode,
                'titulo' => $book->itemname,
                'isbn2' => $book->isbn2,
                'status' => 1
            ]);
        }

        public function saveImage($image,$isbn)
        {
            $imagen = file_get_contents($image);
            $path ="img/";
            file_put_contents($path.$isbn.".jpg", $imagen);
            header('Content-Type: image/png');
        }

        /**
         * Funcion que se encargar de validar las reseñias ingresadas.
         *
         * status =
         * 0: COdiog para buscar
         * 1: Sin isbn,
         * 2: Resenias ya ingresada,
         * 3:Resenias encontrada api
         * 4: Codigo buscado , no encoentrado
         */

        public function validar()
        {
            $inicio = 0;
            $fin = 16505;
            $sinopsisExistentes = [];
            $contador = 0;
            $codigosLibros = Codigos::where('id','>',$inicio)
                ->where('id','<',$fin)
                ->get();

            foreach ($codigosLibros as $item)
            {
                if ($item->isbn2 == null)
                {
                    $item->status = 1;
                }
                else
                    {

                        $sinopsis = Resenias::where('isbn2', $item->isbn2)->first();
                        if (!empty($sinopsis))
                        {
                            $sinopsisExistentes[] = $item->isbn2;
                            $item->status = 2;
                            $contador = $contador +1 ;
                        }
                    }
                    $item->save();
            }
            return $contador;
        }


        public function getSinopsis()
        {
            $libros = Codigos::where('status',0)
                ->where('id','>',199)
                ->where('id','<',1000)
                ->get();
            //$libros = Codigos::where('id',341)->get();
            foreach ($libros as $libro)
            {
                $findSinopsis = $this->getDataFromLibrosMexico($libro->isbn2);
                if ($findSinopsis[0]['encabezado'][1]['codigo'] == 200)
                {
                    $sinopsis = $this->saveSinopsis($findSinopsis[0]['cuerpo']['sinopsis'],$libro);
                    $libro->status =3;
                    if (!empty($findSinopsis[0]['cuerpo']['img_grande']))
                    {
                        $portada = $this->saveImage($findSinopsis[0]['cuerpo']['img_grande'],$libro->isbn2);
                        $libro->status_imagen = 1;
                    }
                }else
                {
                    $libro->status = 4;
                }
                $libro->save();
            }
        }

}


