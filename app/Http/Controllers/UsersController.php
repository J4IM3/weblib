<?php

namespace App\Http\Controllers;
use App\Http\Repositories\UsersRepo;
use App\User;
use App\Http\Requests\CreateUsersFormRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{

    private $usersRepo;

    public function __construct(UsersRepo $usersRepo)
    {
            $this->usersRepo = $usersRepo;
    }


    public function index()
    {
        return view('users.index');
    }

    public function getUsersAjax()
    {
        $users = User::all();
        return DataTables::of($users)->make(true);
    }

    public function create(CreateUsersFormRequest $request)
    {
        return $this->usersRepo->create($request->all());
    }

    public function edit(Request $request)
    {
        return $this->usersRepo->edit($request->all());
    }

    public function changeStatus(Request $request)
    {
        return $this->usersRepo->changeStatus($request->get('id'));
    }

    
}
