<?php

namespace App\Http\Controllers;

use App\Http\Entities\Articulos;
use App\Http\Entities\ArticulosVirtuales;
use App\Http\Entities\Departamentos;
use App\Http\Entities\Papeleria_articulos;
use App\Http\Entities\Resenias;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sinopsis = Resenias::all();
        $totalSinopsis = $sinopsis->count();

        $articulosPapeleria = Papeleria_articulos::all();
        $totalArticulosPapeleria = $articulosPapeleria->count();

        $articulosVirtuales = ArticulosVirtuales::all();
        $totalArticulosVirtuales = $articulosVirtuales->count();

        $departamentos = Departamentos::all();
        $totalDepartamentos = $departamentos->count();


        return view('welcome',compact('totalSinopsis','totalArticulosPapeleria','totalArticulosVirtuales','totalDepartamentos'));
    }
}
