<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-08-14
 * Time: 02:34
 */

namespace App\Http\Services;
class ChangeStatus
{
    public function changeStatus($value)
    {
        if ($value == 0)
        {
            return 1;
        }
        return 0;
    }

}