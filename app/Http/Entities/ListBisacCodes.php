<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class ListBisacCodes extends Model
{
    //List 1.3
    protected  $fillable =['list','frontend','export','code','eng','esp'];
}
