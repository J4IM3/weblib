<?php
//Authenticatable
namespace App\Http\Entities;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class Codigos extends Authenticatable{

    use Notifiable;

    protected $fillable = [
        'id','itemcode','itemname','isbn','status_imagen'
    ];

}
