<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Section_basics extends Model
{
    protected $fillable = ['id', 'code', 'name', 'bs_name'];

    public function seccion_basic()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }

}
