<?php

namespace App\Http\Entities;

use App\ArticlesUnits;
use Illuminate\Database\Eloquent\Model;

class Units extends Model
{
    protected $fillable = ['uomEntry','oumCode','oumName'];

    public function unitName()
    {
        return $this
            ->belongsTo(ArticlesUnits::class);
    }

    public function mainUnit()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }
}
