<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editoriales extends Model
{
    protected $fillable = ['code','name','e_name'];

    public function editoriales()
    {
        return $this->belongsTo(LibreriaArticulos::class);
    }
}
