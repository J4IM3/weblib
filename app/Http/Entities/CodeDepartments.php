<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class codeDepartments extends Model
{
    protected $fillable = ['code','description','status','identifier'];

}
