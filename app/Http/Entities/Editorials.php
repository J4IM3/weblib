<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Editorials extends Model
{
    protected $fillable = ['code','name','e_name'];

    public function editorials()
    {
        return $this->belongsTo(LibreriaArticulos::class);
    }

}
