<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Themas extends Model
{
     protected $fillable = ['code','level1','level2','description_mater','note','level3','level4','level5','level6','level7','level8'];
}
