<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $fillable = ['itemCode','itemName','cardType'];

    public function proveedores()
    {
        return $this->belongsTo(Papeleria_articulos::class);
    }

    public function proveedoresLib()
    {
        return $this->belongsTo(LibreriaArticulos::class);
    }
}
