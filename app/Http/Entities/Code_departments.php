<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Code_departments extends Model
{
    protected $fillable = ['id','code','description','identifier'];

    public function department()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }
}
