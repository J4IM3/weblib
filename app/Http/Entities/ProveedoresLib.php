<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class ProveedoresLib extends Model
{
    protected $fillable = ['cardCode','cardName'];
}
