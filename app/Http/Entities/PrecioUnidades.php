<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class PrecioUnidades extends Model
{
    protected $fillable = ['ItemCode','price','precio_unidad','UomName','menudeo','mayoreo'];

    public function unidadesPrecio()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }
}
