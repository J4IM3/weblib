<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    protected $fillable = ['cardCode','cardName','cardType'];

    public function suppliers()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }
}
