<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{

    protected $fillable = ['id','code','description','identificador'];

    public function department()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }
}
