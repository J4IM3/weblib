<?php

namespace App\Http\Entities;

use App\Http\Entities\ProveedoresLib;
use Illuminate\Database\Eloquent\Model;

class LibreriaArticulos extends Model
{
    protected $fillable =['code','description','autor','price','ean','isbn','isbn-clean',
                          'stock','editorial_id','type','matter_id','bisac','thema_categorie',
                          'code_sat','high_data'];

    public function artImagesLib ()
    {
        return $this
            ->hasMany(ArticulosImages::class,'article_code','code');
    }

    public function sectionsLib()
    {
        return $this
            ->hasOne(LibrarySections::class,'code','matter_id');
    }

    public function editorials()
    {
        return $this
            ->hasOne(Editorials::class,'code','editorial_id');
    }

    public function sinopsis()
    {
        return $this
            ->hasOne(Resenias::class,'codigo','code');
    }

    public function existenciasLib()
    {
        return $this
            ->hasMany(Existencias::class,'Itemcode','code');
    }

    public function proveedoresLib()
    {
        return $this
            ->hasMany(ProveedoresLib::class,'CardCode','cardCode');
    }

}
