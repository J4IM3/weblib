<?php

namespace App\Http\Entities;

use App\ArticlesUnits;
use Illuminate\Database\Eloquent\Model;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

class Papeleria_articulos extends Model
{
    protected $fillable =['codigo_evi','codigo_tl','descripcion','descripcion_alterna','departamento_id','proveedor','unidad_id','fecha_alta','status'];

    public function department()
    {
        return $this
            ->hasOne(Departamentos::class,'code','departamento_id');
    }

    public function seccion_basic()
    {
        return $this
            ->hasOne(Section_basics::class,'code','section');
    }

    public function artImages()
    {
        return $this
            ->hasMany(ArticulosImages::class,'article_code','code');
    }

    public function artImagesUnique()
    {
        return $this
            ->hasOne(ArticulosImages::class,'article_code','code');
    }

    public function artUnits()
    {
        return $this
            ->hasMany(ArticlesUnits::class,'code','code');
    }

    public function mainUnit()
    {
        return $this
            ->hasOne(Units::class,'uomCode','main_unit');
    }

    public function suppliers()
    {
        return $this
            ->hasOne(Suppliers::class,'cardCode','proveedor');
    }

    public function precios()
    {
        return $this
            ->hasOne(Precios::class,'itemcode','code');
    }

    public function unidadesPrecio()
    {
        return $this
            ->hasMany(PrecioUnidades::class,'ItemCode','code');
    }

    public function existencias()
    {
        return $this
            ->hasMany(Existencias::class,'Itemcode','code');
    }

    public function proveedores()
    {
        return $this
            ->hasOne(Proveedores::class,'cardCode','proveedor');
    }

    public function reseniasPape()
    {
        return $this
            ->hasOne(Resenias::class,'codigo','codigo_tl');
    }



}
