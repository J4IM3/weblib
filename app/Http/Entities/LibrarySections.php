<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class LibrarySections extends Model
{
    protected $fillable = ['code','name','sl_name'];

    public function sectionsLib()
    {
        return $this
            ->belongsTo(LibreriaArticulos::class);
    }
}
