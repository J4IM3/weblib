<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Existencias extends Model
{
    protected  $fillable = ['ItemCode','itemName','OnHand','sucursal'];

    public function existencias()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }

    public function existenciasLib()
    {
        return $this
            ->belongsTo(LibreriaArticulos::class);
    }
}
