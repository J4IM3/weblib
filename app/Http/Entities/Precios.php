<?php

namespace App\Http\Entities;
use Illuminate\Database\Eloquent\Model;

class Precios extends Model
{
    protected  $fillable = ['itemCode','menudeo','mayoreo','menudeo_puebla','mayoreo_puebla'];

    public function precios()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }
}
