<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class ArticulosImages extends Model
{
    protected $fillable = ['article_code','url_image'];

    public function artImages()
    {
        return $this
            ->belongsTo(ArticulosImages::class);
    }
    public function artImagesUnique()
    {
        return $this
            ->belongsTo(ArticulosImages::class);
    }

    public function artImagesLib()
    {
        return $this
            ->belongsTo(ArticulosImages::class);
    }


}
