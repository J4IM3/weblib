<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class ArticulosUpdateTemp extends Model
{
    protected $table = "articulos_update_temps";
    protected $fillable = ['codigo_tl','codigo_evi','descripcion_alterna','departamento_id','unidad_id'];
}
