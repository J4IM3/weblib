<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/22/19
 * Time: 10:42 PM
 */
namespace App\Http\Entities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Resenias extends Model {
    use Notifiable;

    protected $fillable = [
        'id','isbn','sinopsis','status','isbn2','codigo','titulo','type_article'
    ];

    public function sinopsis()
    {
        return $this
            ->belongsTo(LibreriaArticulos::class);
    }

    public function reseniasPape()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }


}
