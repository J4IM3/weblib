<?php

namespace App;

use App\Http\Entities\Papeleria_articulos;
use App\Http\Entities\Units;
use Illuminate\Database\Eloquent\Model;

class ArticlesUnits extends Model
{
    protected  $fillable = ['code','oumEntry','oumType'];

    public function artUnits()
    {
        return $this
            ->belongsTo(Papeleria_articulos::class);
    }

    public function unitName()
    {
        return $this
            ->hasOne(Units::class,'oumEntry','oumEntry');
    }
}
